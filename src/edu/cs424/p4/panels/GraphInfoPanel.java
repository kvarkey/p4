package edu.cs424.p4.panels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import org.joda.time.DateTime;

import javax.xml.stream.events.StartDocument;

import processing.core.PConstants;
import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.MainPanel;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.ListPanel;
import edu.cs424.p4.central.components.Slider;
import edu.cs424.p4.db.DBAider;
import edu.cs424.p4.db.GraphFilterParameters;
import edu.cs424.p4.db.GraphPlotData;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.PubSub.Event;
import edu.cs424.p4.pubsub.Subscribe;
import static edu.cs424.p4.central.AppConstants.*;

public class GraphInfoPanel extends Panel implements TouchEnabled,Subscribe
{
	Event updateID;
	EnumColor graphColor;

	boolean showOnMap = true;

	Button showPplButton,saveNewUser,goBackButton,hideMarkers,showMarkers,saveQuery;
	ListPanel uniqueUserPanel, userTweetPanel;

	boolean showPeopleData = false;
	boolean showKeyboard = false;

	/** data start*/
	String startDate = "some" , endDate = "date";
	String keywords,joinOperator;
	String people, tweetsForUserID = "";
	String tod;

	GraphPlotData plotData;
	/**data end */

	public GraphInfoPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0,Event updateID)
	{
		super(x0, y0, width, height, parentX0, parentY0);
		this.updateID = updateID;

		if( this.updateID == Event.UPDATE_GRAPH1)
		{
			graphColor  = EnumColor.GRAPH1;
		}
		else
		{
			graphColor  = EnumColor.GRAPH2;
		}
	}

	@Override
	public void receiveNotification(Event eventName, Object... object)
	{
		if( eventName == updateID )
		{
			GraphFilterParameters filterParameters = (GraphFilterParameters) object[0];
			keywords = "";
			joinOperator = filterParameters.getJoinOperator();

			for( String key : filterParameters.getKeywords() )
			{
				keywords = keywords +" " + key + " " + filterParameters.getJoinOperator();
			}

			if( !keywords.equals("") )
				keywords = keywords.substring(0, keywords.lastIndexOf(" "));

			people = "";
			for( String ppl : filterParameters.getPeople())
			{
				people = people + ppl + ",";
			}

			tod = filterParameters.getTimeOfDay();

			startDate = Slider.currentMinDate.toString().substring(0,10);
			endDate = Slider.currentMaxDate.toString().substring(0,10);

			getGraphData();
		}
		else if( eventName == Event.UPDATE_SLIDER )
		{
			startDate = Slider.currentMinDate.toString().substring(0,10);
			endDate = Slider.currentMaxDate.toString().substring(0,10);
			getGraphData();
		}
	}

	private void getGraphData( )
	{
		// use the filter data and the data from slider to get these values
		plotData = DBAider.getUniqueUser(keywords,people,joinOperator,tod,updateID);
		if(showPeopleData)
			populateSecondWindow();
		
		MainPanel.mapPanel.addMarker();
		
		if( updateID == Event.UPDATE_GRAPH1 && InfoPanel.selectedButton == 0)
			MainPanel.graphPanel.getData1();
		else if(updateID == Event.UPDATE_GRAPH2  && InfoPanel.selectedButton == 1)
			MainPanel.graphPanel.getData2();
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) 
	{
		// if its showing the second screen
		if(showPeopleData)
		{
			if(saveNewUser.containsPoint(x, y))
			{
				if ( !uniqueUserPanel.getSelectedList().isEmpty() )
					PubSub.publishEvent(Event.SAVE_USER, uniqueUserPanel.getSelectedList().toArray()[0]);
			}
			else if(goBackButton.containsPoint(x, y))
			{
				showPeopleData = false;
			}
			else if( uniqueUserPanel.containsPoint(x, y) )
			{
				uniqueUserPanel.touch(x, y, event);
				if ( !uniqueUserPanel.getSelectedList().isEmpty() && !uniqueUserPanel.getSelectedList().toArray()[0].equals(tweetsForUserID) )
				{
					tweetsForUserID = (String) uniqueUserPanel.getSelectedList().toArray()[0];
					userTweetPanel.setRowData(DBAider.getUserTweets(tweetsForUserID,updateID));
					MainPanel.mapPanel.highlightMarker(new Integer( tweetsForUserID.split(" : ")[0]));
				}
			}
			else if( userTweetPanel.containsPoint(x, y))
			{
				userTweetPanel.touch(x, y, event);
			}
		}

		else
		{
			if ( showPplButton.containsPoint(x, y) && plotData.getUniqueUsersList().size() >0 )
			{
				showPeopleData = true;
				populateSecondWindow();
			}
			if( showMarkers.containsPoint(x, y) || hideMarkers.containsPoint(x, y))
			{
				if(showOnMap)
				{
					if( updateID == Event.UPDATE_GRAPH1 )
						NewMapPanel.G1_SHOW = false;
					if( updateID == Event.UPDATE_GRAPH2 )
						NewMapPanel.G2_SHOW = false;
					
				}
				else
				{
					if( updateID == Event.UPDATE_GRAPH1 )
						NewMapPanel.G1_SHOW = true;
					else
						NewMapPanel.G2_SHOW = true;

				}				
				showOnMap = !showOnMap;				
			}
			else if( saveQuery.containsPoint(x, y) )
			{

			}

			else if ( plotData.getUniqueUsersList().size() == 0 )
				System.out.println("GraphInfoPanel.touch() " + "therea r no users to show");
		}


		return false;
	}

	private void populateSecondWindow()
	{
		uniqueUserPanel = new ListPanel(uniqueUsersX,uniqueUsersY , uniqueUsersWidth, uniqueUsersHeight, x0, y0, 5,
				plotData.getUniqueUsersList(), false, graphColor);
		uniqueUserPanel.setDefaultSelected(0);				
		uniqueUserPanel.setup();
		saveNewUser.setVisibilty(true);		

		if ( !uniqueUserPanel.getSelectedList().isEmpty() && !uniqueUserPanel.getSelectedList().toArray()[0].equals(tweetsForUserID) )
		{
			tweetsForUserID = (String) uniqueUserPanel.getSelectedList().toArray()[0];

			userTweetPanel = new ListPanel(userTweetsX,userTweetsY,userTweetsWidth,userTweetsHeight,x0,y0,3,DBAider.getUserTweets(tweetsForUserID,updateID),false,graphColor);
			userTweetPanel.setup();
		}
	}

	@Override
	public void setup()
	{
		PubSub.subscribeEvent(updateID, this);
		PubSub.subscribeEvent(Event.UPDATE_SLIDER, this);

		showPplButton = new Button(showUsersX, showUsersY, showUsersWidth, showUsersHeight, x0, y0, "Show User data", true);
		showPplButton.setup();

		hideMarkers = new Button(showUsersX, showUsersY-20, showUsersWidth, showUsersHeight, x0, y0, "Hide Markers", true);
		hideMarkers.setup();

		showMarkers = new Button(showUsersX, showUsersY-20, showUsersWidth, showUsersHeight, x0, y0, "Show Markers", true);
		showMarkers.setup();

		saveQuery = new Button(showUsersX, showUsersY-40, showUsersWidth, showUsersHeight, x0, y0, "Show Query", true);
		saveQuery.setup();

		saveNewUser = new Button(saveUserX, saveUserY,saveUserWidth, saveUserHeight, x0, y0, "Save User", false);
		saveNewUser.setup();

		goBackButton = new Button(goBackGraphInfoX, goBackGraphInfoY, goBackGraphInfoWidth, goBackGraphInfoHeight, x0, y0, "Go Back", false);
		goBackButton.setup();
	}

	@Override
	public void draw() 
	{
		pushStyle();
		background(EnumColor.GRAY_ODD);

		if(showPeopleData) // show second screen
		{

			showPplButton.setVisibilty(false);
			saveQuery.setVisibilty(false);

			uniqueUserPanel.draw();
			userTweetPanel.draw();

			saveNewUser.setVisibilty(true);
			goBackButton.setVisibilty(true);

			saveNewUser.draw();
			goBackButton.draw();

			if(showKeyboard)
			{

			}

			textSize(12);	
			textAlign(PConstants.LEFT,PConstants.CENTER);
			text("User Info",graphInfoLabelX, graphInfoLabelY);
		}
		else // show general info
		{
			fill(EnumColor.BLACK);
			saveNewUser.setVisibilty(false);
			goBackButton.setVisibilty(false);

			if( keywords != null)
			{
				textAlign(PConstants.LEFT,PConstants.TOP);

				//text("Filters Applied",graphInfoLabelX, graphInfoLabelY);
				text( "Keywords : "+ keywords, graphAttributeX, graphAttributeY,width-12,15 );
				text("People : " + people, graphAttributeX, graphAttributeY + (20)*1,width-12,15 );
				text("Start Date : " + startDate + " " + "End Date : " + endDate, graphAttributeX, graphAttributeY + (20)*2,width-12,15 );
				text("Tweets with Keyords : " + plotData.getTweetswithKeywords(), graphAttributeX, graphAttributeY + (20)*3,width-12,15 );
				text("Total Tweets : " + plotData.getTotalTweets(), graphAttributeX, graphAttributeY + (20)*4,width-12,15 );
				text("Unique Users : " + plotData.getUniqueUsers(), graphAttributeX, graphAttributeY + (20)*5,width-12,15 );				

				showPplButton.setVisibilty(true);
				showPplButton.draw();

				if(showOnMap)
				{
					hideMarkers.setVisibilty(true);
					showMarkers.setVisibilty(false);
					hideMarkers.draw();
				}
				else
				{
					showMarkers.setVisibilty(true);
					hideMarkers.setVisibilty(false);
					showMarkers.draw();
				}

				saveQuery.setVisibilty(false);
				saveQuery.draw();

				textSize(12);	
				textAlign(PConstants.LEFT,PConstants.CENTER);
				text("Filters Applied",graphInfoLabelX, graphInfoLabelY);			

			}
			else
			{				
				textAlign( PConstants.CENTER, PConstants.CENTER );
				text("No Filter Applied", width/2, height/2);
			}
		}

		popStyle();

	}

}
