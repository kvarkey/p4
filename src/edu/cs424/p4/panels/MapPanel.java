package edu.cs424.p4.panels;

import static edu.cs424.p4.central.AppConstants.mapHeight;
import static edu.cs424.p4.central.AppConstants.mapWidth;
import static edu.cs424.p4.central.AppConstants.mapX;
import static edu.cs424.p4.central.AppConstants.mapY;

import java.util.Hashtable;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.EnumConfig;
import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.SettingsLoader;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.map.Location;
import edu.cs424.p4.central.components.map.MercatorMap;


public class MapPanel extends Panel implements TouchEnabled {
	PImage mapImage;
	PVector lastTouchPos = new PVector();
	PVector lastTouchPos2 = new PVector();
	int touchID1;
	int touchID2;
	@SuppressWarnings("rawtypes")
	Hashtable touchList;
	float sc;
	float tx, ty;
	float screenx, screeny;
	MercatorMap mmap;
	float mapx1, mapy1, mapx2, mapy2;

	PVector initTouchPos = new PVector();
	PVector initTouchPos2 = new PVector();
	private int imageCenterX;
	private int imageCenterY;
	private int zoomWidth;
	private int zoomHeight;

	// all required coordinates and step values
	float translateY = 10, translateX = 30;

	float currentCenterX, currentCenterY;
	float currentTopLeftX, currentBottomRightX, currentTopLeftY,
			currentBottomRightY;

	int zoomLevel = 1, noMoreZoomIn = 5, noMoreZoomOut = 1;
	Location[] boundaryLocations = new Location[2];

	@SuppressWarnings("rawtypes")
	public MapPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) {
		super(x0, y0, width, height, parentX0, parentY0);
		touchList = new Hashtable();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean touch(int ID, float x, float y, MouseMovements event) {

		if (this.containsPoint(x, y)) {
			if (event == MouseMovements.MOUSEDOWN) {
				mapMouseDown(ID, x, y);
			} else if (event == MouseMovements.MOUSEMOVE) {
				mapMouseMove(ID, x, y);
			} else // MOUSEUP
			{
				touchList.remove(ID);
			}
		}

		System.out.println("MapPanel.touch()");
		return false;
	}

	@SuppressWarnings("unchecked")
	private void mapMouseMove(int ID, float x, float y) {
		if (touchList.size() < 2) {
			if (sc > 1) {
				// Only one touch, drag map based on last position
				this.tx += (x - lastTouchPos.x) / this.sc;
				this.ty += (y - lastTouchPos.y) / this.sc;
			} else if (sc < 1) {
				sc = 1;
				tx = 0;
				ty = 0;
			}
		} else if (touchList.size() == 2) {
			if (bestZoomForScale(sc)<=noMoreZoomOut && bestZoomForScale(sc)>=noMoreZoomIn) {
				// Only two touch, scale map based on midpoint and distance from
				// initial touch positions

				float sc = Main.main.dist(lastTouchPos.x, lastTouchPos.y,
						lastTouchPos2.x, lastTouchPos2.y);
				float initPos = Main.main.dist(initTouchPos.x, initTouchPos.y,
						initTouchPos2.x, initTouchPos2.y);

				PVector midpoint = new PVector(
						(lastTouchPos.x + lastTouchPos2.x) / 2,
						(lastTouchPos.y + lastTouchPos2.y) / 2);
				sc -= initPos;
				sc /= 5000;
				sc += 1;
				float mx = (midpoint.x - x0) - this.width / 2;
				float my = (midpoint.y - y0) - this.height / 2;
				this.tx -= mx / this.sc;
				this.ty -= my / this.sc;
				this.sc *= sc;
				this.tx += mx / this.sc;
				this.ty += my / this.sc;

				if (sc < 1)
					sc = 1;
			}

		}

		// Update touch IDs 1 and 2
		if (ID == touchID1) {
			lastTouchPos.x = x;
			lastTouchPos.y = y;
		} else if (ID == touchID2) {
			lastTouchPos2.x = x;
			lastTouchPos2.y = y;
		}

		// Update touch list
		Touch t = new Touch(ID, x, y, 0, 0);
		touchList.put(ID, t);
	}

	/**
	 * Handles mouse down on the map.
	 * 
	 * @param ID
	 * @param x
	 * @param y
	 */
	@SuppressWarnings("unchecked")
	private void mapMouseDown(int ID, float x, float y) {
		if (x > s(mapX) && x < s(mapWidth) && y > s(mapY) && y < s(mapHeight)) // Make
																				// sure
																				// everything
																				// is
																				// inside
																				// map.
		{
			// Update the last touch position
			lastTouchPos.x = x;
			lastTouchPos.y = y;

			// Add a new touch ID to the list
			Touch t = new Touch(ID, x, y, 0, 0);
			touchList.put(ID, t);

			if (touchList.size() == 1) { // If one touch record initial position
											// (for dragging). Saving ID 1 for
											// later
				touchID1 = ID;
				initTouchPos.x = x;
				initTouchPos.y = y;
			} else if (touchList.size() == 2) { // If second touch record
												// initial position (for
												// zooming). Saving ID 2 for
												// later
				touchID2 = ID;
				initTouchPos2.x = x;
				initTouchPos2.y = y;
			}
		}
	}

	@Override
	public void setup() {
		this.imageCenterX = mapWidth / 2;
		this.imageCenterY = mapHeight / 2;
		this.zoomWidth = mapWidth;
		this.zoomHeight = mapHeight;

		sc = 1.0f;
		tx = 0;
		ty = 0;
		zoomLevel = bestZoomForScale(sc);
		System.out.println("init zoom level :"+zoomLevel);
		mapImage = loadImage(SettingsLoader.dir
				+ SettingsLoader.getConfigValueAsString(EnumConfig.MAPIMAGE));
		mmap = new MercatorMap(0,0,this.width, this.height, 42.3017f, 42.1609f,
				93.5673f, 93.1923f);
	}

	@Override
	public void draw() {
		pushStyle();
		background(EnumColor.BLACK);
		imageMode(PApplet.CENTER);
		drawMap();
		popStyle();

	}
	
	public void drawMap()
	{
		 transScale(); 
		 zoomLevel = bestZoomForScale(sc);
		 Location l = new Location(42.2183f,93.38957f); 
		 PVector
		 p = this.toMapPoint(l); 
		 drawEllipse(p.x,p.y,10); 
		 
		 Main.main.fill(24);
		 rect(x0+width,y0,Main.main.getWidth(),Main.main.getHeight());
		 //drawMapImage();
		 //image(mapImage,x0,y0,x0+width,y0+height);
		 
	}

	public void transScale() {
		float x1 = 0, y1 = 0, s1 = sc;
		if (tx != 0 && ty != 0) {
			x1 = x0 + width / 2;
			y1 = y0 + height / 2;
			s1 = sc;
			x1 += tx;
			y1 += ty;
		}
		image(mapImage, x0 + x1, y0 + y1, width * s1, height * s1);
	}

	public void drawMapImage() {
		image(mapImage, imageCenterX, imageCenterY, zoomWidth, zoomHeight);
	}

	public void drawEllipse(float x, float y, float w) {

		pushStyle();
		stroke(0);
		fill(0);
		float x1 = 0, y1 = 0;
		if (tx != 0 && ty != 0) {
			x1 = x0 + width / 2;
			y1 = y0 + height / 2;
			x1 += tx;
			y1 += ty;
		}
		ellipse(x + x1, y + y1, 10, 10);
		popStyle();
	}

	public PVector toMapPoint(Location p) {
		return mmap.getScreenLocation(new PVector(p.lat, p.lon));
	}
	
	public int bestZoomForScale(float scale) {
	    return (int)Main.main.min(20, Main.main.max(1, (int)Main.main.round(Main.main.log(scale) / Main.main.log(2))));
	  }

}

class Touch {
	float xPos;
	float yPos;
	float xWidth;
	float yWidth;
	int ID;

	Touch(int id, float x, float y, float w, float h) {
		ID = id;
		xPos = x;
		yPos = y;
		xWidth = w;
		yWidth = h;
	}
}// class

