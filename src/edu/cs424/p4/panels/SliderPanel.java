package edu.cs424.p4.panels;

import processing.core.PImage;
import edu.cs424.p4.central.EnumConfig;
import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.SettingsLoader;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.Slider;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.PubSub.Event;

/**
 * Class that sets up, draws and listens for events of the Slider.
 * Use dateSlider.currentMinDate and dateSlider.currentMaxDate to 
 * access the current min and max date selected on the slider.
 * @author GV
 *
 */
public class SliderPanel extends Panel implements TouchEnabled
{
	Slider dateSlider;
	PImage windImage;

	public SliderPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) {
		super(x0, y0, width, height, parentX0, parentY0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		//dateSlider.updateSlider(x, y);
		return false;
	}
	
	
	public boolean touch(int ID, float x, float y, MouseMovements event) {
		
		if(event == MouseMovements.MOUSEDOWN)
			PubSub.publishEvent(Event.UPDATE_SLIDER, null);
		
		dateSlider.updateSlider(ID, x, y);
		return false;
	}

	@Override
	public void setup() {
		// TODO Auto-generated method stub
		dateSlider = new Slider(x0Zoom,y0Zoom,x0Zoom+s(width),y0Zoom+s(height));
		windImage = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.WINDIMAGE));
	}
	
	public void draw(){
		//image(windImage, x0, y0, width, height*0.65f);
		dateSlider.drawSlider();
		
		Main.main.fill(0);
	}

}