package edu.cs424.p4.panels;

import java.util.ArrayList;

import processing.core.PConstants;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.Keyboard;
import edu.cs424.p4.central.components.ListPanel;
import edu.cs424.p4.db.DBAider;
import edu.cs424.p4.db.GraphFilterParameters;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.PubSub.Event;
import edu.cs424.p4.pubsub.Subscribe;
import static edu.cs424.p4.central.AppConstants.*;

public class GraphFilterPanel extends Panel implements TouchEnabled,Subscribe
{
	Event graphID;
	ListPanel keyword, people, time , query ;
	Keyboard keyboard;
	int selectedFilter;
	Button and,or,apply;

	String joinOperator = "AND";

	public GraphFilterPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0, Event graphID) 
	{
		super(x0, y0, width, height, parentX0, parentY0);
		this.graphID = graphID;

	}

	@Override
	public boolean touch(float x, float y, MouseMovements event)
	{
		if( selectedFilter == 0 && keyword.containsPoint(x, y) ) // draw keyword
		{
			keyword.touch(x, y, event);
		}
		else if( selectedFilter == 1 && people.containsPoint(x, y) ) // draw ppl
		{
			people.touch(x, y, event);			

		}
		else if(selectedFilter == 2 && keyboard.containsPoint(x, y) )
		{
			keyboard.touch(x, y, event);
		}
		else if(selectedFilter == 3 && time.containsPoint(x, y) )
		{
			time.touch(x, y, event);
		}
		else if(selectedFilter == 4 && query.containsPoint(x, y) )
		{
			query.touch(x, y, event);
		}

		else if( and.containsPoint(x, y) && (!and.isPressed) )
		{
			and.setPressed(true);
			or.setPressed(false);
			joinOperator = "AND";
		}
		else if( or.containsPoint(x, y) && (!or.isPressed) )
		{
			or.setPressed(true);
			and.setPressed(false);
			joinOperator = "OR";
		}

		if( apply.containsPoint(x, y) )
		{
			System.out.println( joinOperator + " " + keyword.getSelectedList() );
			System.out.println( people.getSelectedList() );


			String tod;
			if(time.getSelectedList() == null)
				tod = null;
			else if( time.getSelectedList().isEmpty())
				tod  = null;
			else
				tod = (String) time.getSelectedList().toArray()[0];

			GraphFilterParameters toSend = new GraphFilterParameters( keyword.getSelectedList(), people.getSelectedList() ,joinOperator ,tod );			

			if ( graphID == Event.FOCUS_GRAPH1 )
				PubSub.publishEvent(Event.UPDATE_GRAPH1, toSend);
			else
				PubSub.publishEvent(Event.UPDATE_GRAPH2, toSend);
		}


		return false;
	}

	@Override
	public void setup() 
	{

		EnumColor selectedColor;
		PubSub.subscribeEvent(Event.NEW_USER_ADDED, this);

		if(graphID == Event.FOCUS_GRAPH1)
			selectedColor = EnumColor.GRAPH1;
		else
			selectedColor = EnumColor.GRAPH2;

		keyword = new ListPanel(filterKeywordListX, filterKeywordListY, filterKeywordWidth, filterKeywordHeight, x0, y0, 5, getKeyowrdList() , true , selectedColor);
		keyword.setup();

		people =  new ListPanel(filterKeywordListX, filterKeywordListY, filterKeywordWidth, filterKeywordHeight, x0, y0, 5, null , true , selectedColor);
		people.setup();

		time = new ListPanel(filterKeywordListX, filterKeywordListY, filterKeywordWidth, filterKeywordHeight, x0, y0, 5, getTimeOfdayList() , false , selectedColor);
		time.setup();

		query = new ListPanel(filterKeywordListX, filterKeywordListY, filterKeywordWidth, filterKeywordHeight, x0, y0, 5, getQuery() , false , selectedColor);
		query.setup();
		

		keyboard = new Keyboard(20, 20, 250, 105+25, x0,y0);
		keyboard.setup();

		and = new Button(andButtonX, andButtonY, andButtonWidth, andButtonHeight, x0, y0, "AND", true);
		and.setRoundedCorners(false);
		and.setPressed(true);		
		and.setup();

		or = new Button(orButtonX, orButtonY, andButtonWidth, andButtonHeight, x0, y0, "OR", true);
		or.setRoundedCorners(false);
		or.setup();

		apply = new Button(applyButtonX, applyButtonY, andButtonWidth, andButtonHeight, x0, y0, "Apply", true);
		apply.setRoundedCorners(false);
		apply.setup();

		updatePplList();

	}


	public void draw(int selectedFilter)
	{
		pushStyle();
		background(EnumColor.BLACK);
		this.selectedFilter = selectedFilter;

		if( selectedFilter == 0 ) // draw keyword
		{
			keyword.draw();
			and.setVisibilty(true);
			or.setVisibilty(true);
			apply.setVisibilty(true);

			textAlign(PConstants.LEFT, PConstants.CENTER);
			textSize(10);	
			text("Select Keyword",graphInfoLabelX, graphInfoLabelY);


		}
		else if( selectedFilter == 1) // draw ppl
		{
			people.draw();
			and.setVisibilty(false);
			or.setVisibilty(false);
			apply.setVisibilty(true);
			
			textAlign(PConstants.LEFT, PConstants.CENTER);
			textSize(10);	
			text("Select Specific People",graphInfoLabelX, graphInfoLabelY);

		}
		else if( selectedFilter == 2) // draw search panel
		{
			keyboard.draw();
			and.setVisibilty(false);
			or.setVisibilty(false);
			apply.setVisibilty(false);
			
			textAlign(PConstants.LEFT, PConstants.CENTER);
			textSize(10);	
			text("Build Custom keyword search",graphInfoLabelX, graphInfoLabelY);

		}
		else if( selectedFilter == 3) // draw time of day
		{
			time.draw();
			and.setVisibilty(false);
			or.setVisibilty(false);
			apply.setVisibilty(true);
			
			textAlign(PConstants.LEFT, PConstants.CENTER);
			textSize(10);	
			text("Select time of Day",graphInfoLabelX, graphInfoLabelY);

		}
		else if( selectedFilter == 4) // draw query 
		{
			query.draw();
			and.setVisibilty(false);
			or.setVisibilty(false);
			apply.setVisibilty(true);
			
			textAlign(PConstants.LEFT, PConstants.CENTER);
			textSize(10);	
			text("Load Saved Query",graphInfoLabelX, graphInfoLabelY);

		}

		and.draw();
		or.draw();
		apply.draw();
		popStyle();
	}


	private void updatePplList()
	{
		people.setRowData(DBAider.getSavedUserNames());
	}

	private ArrayList<String> getTimeOfdayList()
	{
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("morning");
		temp.add("day");
		temp.add("night");

		return temp;
	}


	private ArrayList<String> getQuery()
	{
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("1");
		temp.add("2");
		temp.add("3");

		return temp;
	}



	private ArrayList<String> getKeyowrdList()
	{
		String[]  temp = {

				"feeling",
				"chills",
				"cold",
				"fever",
				"smoke",
				"disaster",
				"game",
				"pain",
				"stomach",
				"causing",
				"weather",
				"headache",
				"conventions",
				"crashing",
				"truck",
				"accident",
				"airplane",
				"medicine",
				"tired",
				"pain",
				"spill",
				"pneumonia",
				"fatigue",
				"diarrhoea",
				"cough",
				"ill",
				"shortness",
				"throat",
				"chill",
				"games",
				"burn",
				"body",
				"bedridden",
				"sickness",
				"upset",
				"coughing",
				"nausea",
				"cancer",
				"hurting",
				"pains",
				"cramps",
				"vomiting",
				"flames",
				"burns",
				"freezing",
				"crashing",
				"sweating",
				"disgusting",
				"virus",
				"stool",
				"gastro",
				"stadium",
				"illness",
				"vomit",
				"freeze",
				"coughed",
				"bile",
				"ulcers",
				"feverish",

		};

		ArrayList<String> toReturn = new ArrayList<String>();

		for( String t : temp)
			toReturn.add(t);

				return toReturn;

	}

	@Override
	public void receiveNotification(Event eventName, Object... object)
	{
		if( eventName == Event.NEW_USER_ADDED )
		{
			updatePplList();
		}
	}

}
