package edu.cs424.p4.panels;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;

public class EventInfoPanel extends Panel implements TouchEnabled {

	public EventInfoPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) {
		super(x0, y0, width, height, parentX0, parentY0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setup() {
		// TODO Auto-generated method stub
		
	}
	
	public void draw(){
		pushStyle();
		background(EnumColor.BLACK, 200);
		popStyle();
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		// TODO Auto-generated method stub
		return false;
	}

}
