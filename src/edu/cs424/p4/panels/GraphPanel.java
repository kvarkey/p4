package edu.cs424.p4.panels;

import java.security.KeyStore.LoadStoreParameter;
import java.util.ArrayList;
import de.bezier.data.sql.SQLite;
import processing.core.PConstants;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PShape;
import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.SettingsLoader;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.Slider;
import edu.cs424.p4.db.DBAider;
import static edu.cs424.p4.central.AppConstants.*;

public class GraphPanel extends Panel implements TouchEnabled
{
	protected int noOfInterval;
	protected SQLite db;
	protected float xMin, xMax;
	protected float yMin, countMax;
	protected int noOfKeywords;
	protected ArrayList<String> keyword;
	ArrayList<Integer> count;
	ArrayList<Integer> date;
	ArrayList<ArrayList<Integer>> countArray;
	ArrayList<ArrayList<Integer>> dateArray;
	ArrayList<Integer> colorArray;
	Button infoButton;
	EventInfoPanel eventInfoPanel;
	boolean eventInfoPanelFlag;
	boolean drawGraph1 = false;
	boolean drawGraph2 = false;
	String day = "Day(9AM-6PM)";
	String morning = "Morning(12AM-9AM)";
	String night = "Night(6PM-12AM)";
	
	
	//LegendPanel legendPanel;
	
	
	
	public GraphPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) {
		super(x0, y0, width, height, parentX0, parentY0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		// TODO Auto-generated method stub
//		if( event == MouseMovements.MOUSEDOWN){
//			//System.out.println("in mousedown");
//			if(infoButton.containsPoint(x, y)){
//				//System.out.println("in button click");
//				eventInfoPanelFlag = true;
//				return false;
//			}
//			if(eventInfoPanelFlag){
//				eventInfoPanelFlag = false;
//			}
//		}
		return false;
	}

	@Override
	public void setup() 
	{
		// TODO Auto-generated method stub
		db = new SQLite(Main.main, "example.db");
		count = new ArrayList<Integer>();
		date = new ArrayList<Integer>();
		keyword = new ArrayList<String>();
		countArray = new ArrayList<ArrayList<Integer>>();
		dateArray =  new ArrayList<ArrayList<Integer>>();
		colorArray = new ArrayList<Integer>();
		eventInfoPanel = new EventInfoPanel(0, 0, graphWidth, graphHeight, x0, y0);
		eventInfoPanel.setup();
		addTouchSubscriber(eventInfoPanel);
		count.add(100);
		count.add(13);
		count.add(700);
		count.add(23);
		count.add(500);
		count.add(200);
		count.add(40);
		count.add(100);
		date.add(1);
		date.add(2);
		date.add(3);
		date.add(4);
		date.add(5);
		date.add(6);
		date.add(7);
		date.add(8);
		countArray.add(0,count);
		dateArray.add(0,date);
		//System.out.println("datearray sixe"+dateArray.get(0).size());
		noOfInterval = date.size();
		ArrayList<Integer> count2 = new ArrayList<Integer>();
		count2.add(13);
		count2.add(100);
		count2.add(500);
		count2.add(32);
		count2.add(5);
		count2.add(600);
		count2.add(7);
		count2.add(8);
		countArray.add(1,count2);
		noOfKeywords = countArray.size();
		keyword.add("accident");
		keyword.add("fire");
		colorArray.add(0xFFCCCCCC);
		colorArray.add(0xFFCCCC99);
		colorArray.add(0xFFFFFF66);
		colorArray.add(0xFF99FF99);
		colorArray.add(0xFF99FFFF);
		colorArray.add(0xFF9999FF);
		colorArray.add(0xFFCCCC99);
		colorArray.add(0xFF9999FF);
		colorArray.add(0xFF99FF99);
		getData();
		
	}
	
	@Override
	public void draw()
	{
		
		background(EnumColor.GRAY);
		//System.out.println("init datearraysize:"+dateArray.get(0).size());
		if (Slider.currentMinDate.compareTo(Slider.currentMaxDate) != 0 ){
			if (drawGraph1 || drawGraph2){
			drawAxis();
			getCountMax();
			drawAxisLabels();
			pushStyle();
			strokeWeight(4);
			drawData();
			popStyle();
			}
		}
		else{
			drawPie();
			drawLegend();
		}
		
		if(eventInfoPanelFlag){
			//System.out.println("anything");
			eventInfoPanel.draw();
		}
		//System.out.println("in graphPaneldraw()");
	}
	
	private void drawPie() {
		// TODO Auto-generated method stub
		//System.out.println(countArray.get(0));
		if(drawGraph1){
            drawPie1();
            drawLegend();
    }
    else if(drawGraph2){
            drawPie2();
            drawLegend();
    }
//		drawPie1();
//		if(countArray.size() > 1){
//			drawPie2();
//		}
//		float lastAngle1 = 0;
//		float lastAngle2 = 0;
//		float pieDia = Math.min(width, height) * 0.75f;
//		float total1=0,total2=0;
//		for(int i=0;i<3;i++){
//			total1 += countArray.get(0).get(i);
//			total2 += countArray.get(1).get(i);
//		}
//		float mapVal1 = 0,mapVal2 = 0;
//		for(int i = 0; i < 7; i++){
//			fill(colorArray.get(i));
//			mapVal1 = Main.main.map(countArray.get(0).get(i), 0.0f, total1, 0, 360);
//			mapVal2 = Main.main.map(countArray.get(1).get(i), 0.0f, total1, 0, 360);
//			arc(graphX + (graphWidth/4), graphY + (graphHeight/2),pieDia,pieDia, lastAngle1, lastAngle1+radians(mapVal1));
//			arc(graphX + (graphWidth*3/4), graphY + (graphHeight/2),pieDia,pieDia, lastAngle2, lastAngle2+radians(mapVal1));
//			lastAngle1 += radians(mapVal1);
//			lastAngle2 += radians(mapVal2);
//		}
	}

	private void drawPie2() {
		// TODO Auto-generated method stub
		if(countArray.get(1).size() != 0){
			float lastAngle2 = 0;
			float pieDia = Math.min(width, height) * 0.75f;
			float total2=0;
			for(int i=0;i<3;i++){
				total2 += countArray.get(1).get(i);
			}
			float mapVal2 = 0;
			for(int i = 0; i < 3; i++){
				fill(colorArray.get(i));
				mapVal2 = Main.main.map(countArray.get(1).get(i), 0.0f, total2, 0, 360);
				if(i ==0){
					day += Math.round(Main.main.map(countArray.get(0).get(i), 0, total2, 0, 100))+"%";
				}
				else if(i == 1){
					morning += Math.round(Main.main.map(countArray.get(0).get(i), 0, total2, 0, 100))+"%";
				}
				else if(i == 2){
					night += Math.round(Main.main.map(countArray.get(0).get(i), 0, total2, 0, 100))+"%";
				}
				arc(graphX + (graphWidth*3/4), graphY + (graphHeight/2),pieDia,pieDia, lastAngle2, lastAngle2+radians(mapVal2));
				lastAngle2 += radians(mapVal2);
			}
		}
		ellipse(graphX + (graphWidth*3/4),graphY + (graphHeight/2),Math.min(width, height) * 0.75f,Math.min(width, height) * 0.75f);
	}

	private void drawPie1() {
		// TODO Auto-generated method stub
		if(countArray.get(0).size() != 0){
			float lastAngle1 = 0;
			float pieDia = Math.min(width, height) * 0.75f;
			float total1=0;
			for(int i=0;i<3;i++){
				total1 += countArray.get(0).get(i);
			}
			float mapVal1 = 0;
			for(int i = 0; i < 3; i++){
				fill(colorArray.get(i));
				mapVal1 = Main.main.map(countArray.get(0).get(i), 0.0f, total1, 0, 360);
				if(i ==0){
					day += Math.round(Main.main.map(countArray.get(0).get(i), 0, total1, 0, 100))+"%";
				}
				else if(i == 1){
					morning += Math.round(Main.main.map(countArray.get(0).get(i), 0, total1, 0, 100))+"%";
				}
				else if(i == 2){
					night += Math.round(Main.main.map(countArray.get(0).get(i), 0, total1, 0, 100))+"%";
				}
				arc(graphX + (graphWidth/4), graphY + (graphHeight/2),pieDia,pieDia, lastAngle1, lastAngle1+radians(mapVal1));
				lastAngle1 += radians(mapVal1);
			}
		}
		ellipse(graphX + (graphWidth*3/4),graphY + (graphHeight/2),Math.min(width, height) * 0.75f,Math.min(width, height) * 0.75f);
	}

	private void drawData() {
		// TODO Auto-generated method stub
		//System.out.println("countarray:"+countArray);
		//System.out.println("datearray:"+dateArray);
		if(drawGraph1){
			drawGraph1();
		}
		else if(drawGraph2){
			drawGraph2();
		}
//		if(countArray.size() > 1){
//			drawGraph2();
//		}
//		float interval = (graphWidth-3*graphXBuffer)/noOfInterval;
//		for(int k = 0; k < noOfKeywords; k++){
//			float intervalLoop = 0;
//			noFill();
//			stroke(colorArray.get(k));
//			beginShape();
//			for(int i = 0 ;i<noOfInterval; i++)
//			{
//				//System.out.println(count[i]+":"+Main.main.map(countArray.get(k)[i],0,noOfInterval,graphHeight-2*graphYBuffer,graphYBuffer));
//				vertex(2*graphXBuffer+intervalLoop,Main.main.map(countArray.get(k).get(i),0,countMax,graphHeight-2*graphYBuffer,graphYBuffer));
//				intervalLoop += interval;
//				if(dateArray.get(0).get(i) ==2){
//					infoButton = new Button(2*graphXBuffer+intervalLoop,graphYBuffer,2,graphHeight-3*graphYBuffer,x0,y0,"",true);
//					infoButton.setup();
//					infoButton.draw();
//				}
//					
//			}
//			endShape();
//		}
		stroke(0);
		//drawLegend(/*k*/);
		
	}

	private void drawGraph2() {
		// TODO Auto-generated method stub
		float interval = (graphWidth-3*graphXBuffer)/(noOfInterval-1);
		float intervalLoop = 0;
		noFill();
		stroke(EnumColor.GRAPH2);
		beginShape();
		for(int i = 0 ;i < noOfInterval; i++)
		{
			//System.out.println(count[i]+":"+Main.main.map(countArray.get(k)[i],0,noOfInterval,graphHeight-2*graphYBuffer,graphYBuffer));
			//System.out.println("in datagraph1()");
			vertex(2*graphXBuffer+intervalLoop,Main.main.map(countArray.get(1).get(i),0,countMax,graphHeight-2*graphYBuffer,graphYBuffer));
			intervalLoop += interval;
//			if(dateArray.get(0).get(i) == 2){
//				infoButton = new Button(2*graphXBuffer+intervalLoop,graphYBuffer,2,graphHeight-3*graphYBuffer,x0,y0,"",true);
//				infoButton.setup();
//				infoButton.draw();
//			}
				
		}
		endShape();
		
	}

	private void drawGraph1() {
		// TODO Auto-generated method stub
		float interval = (graphWidth-3*graphXBuffer)/(noOfInterval-1);
		float intervalLoop = 0;
		noFill();
		stroke(EnumColor.GRAPH1);
		beginShape();
		for(int i = 0 ;i < noOfInterval; i++)
		{
			//System.out.println(count[i]+":"+Main.main.map(countArray.get(k)[i],0,noOfInterval,graphHeight-2*graphYBuffer,graphYBuffer));
			//System.out.println("in datagraph1()");
			vertex(2*graphXBuffer+intervalLoop,Main.main.map(countArray.get(0).get(i),0,countMax,graphHeight-2*graphYBuffer,graphYBuffer));
			intervalLoop += interval;
//			if(dateArray.get(0).get(i) == 2){
//				infoButton = new Button(2*graphXBuffer+intervalLoop,graphYBuffer,2,graphHeight-3*graphYBuffer,x0,y0,"",true);
//				infoButton.setup();
//				infoButton.draw();
//			}
				
		}
		endShape();
	}

	private void drawLegend(/*int color*/) {
		pushStyle();
		fill(colorArray.get(0));
		rect(5,graphHeight-20,10,10);
		fill(EnumColor.BLACK);
		textAlign(PConstants.LEFT , PConstants.BOTTOM);
		//text("Day(9 AM-6 PM)",17,graphHeight-8);
		text(day,17,graphHeight-8);
		fill(colorArray.get(1));
		rect(110,graphHeight-20,10,10);
		fill(EnumColor.BLACK);
		textAlign(PConstants.LEFT , PConstants.BOTTOM);
		//text("Morning(12 AM-9 AM)",130,graphHeight-7);
		text(morning,130,graphHeight-7);
		fill(colorArray.get(2));
		rect(230,graphHeight-20,10,10);
		fill(EnumColor.BLACK);
		textAlign(PConstants.LEFT , PConstants.BOTTOM);
		//text("Night(6 PM-12 AM)",250,graphHeight-7);
		text(night,250,graphHeight-7);
		popStyle();

        day = "Day(9AM-6PM)";
        morning = "Morning(12AM-9AM)";
        night = "Night(6PM-12AM)";
		// TODO Auto-generated method stub
//		legendPanel = new LegendPanel(graphWidth-40, 0, 20, graphHeight, x0, y0, keyword, colorArray);
//		legendPanel.draw();
//		rectMode(PConstants.CORNER);
//		fill(colorArray[color]);
//		text(keyword,graphWidth-30,20+(20*color));
	}

	private void getCountMax() {
		// TODO Auto-generated method stub
		if(drawGraph1)
		{
			float max = 0;
			for(int count : countArray.get(0)){
				if(count > max){
					max = count;
				}
			}
			countMax = max;
		}
		if(drawGraph2)
		{
			float max = 0;
			for(int count : countArray.get(0)){
				if(count > max){
					max = count;
				}
			}
			countMax = max;
		}
//		float max = 0;
//		for(ArrayList<Integer> i : this.countArray)
//			for(int count : i){
//				if(count > max){
//					max = count;
//				}
//			}
//		countMax = max;
	}

	public void getData1(){
		this.countArray = DBAider.countArray;
		this.dateArray = DBAider.dateArray;
		this.drawGraph1 = true;
		this.drawGraph2= false;
		noOfInterval = dateArray.get(0).size();
		if(noOfInterval == 1){
			noOfInterval = 2;
		}
	}
	public void getData2(){
		this.countArray = DBAider.countArray;
		this.dateArray = DBAider.dateArray;
		this.drawGraph2 = true;
		this.drawGraph1 = false;
		noOfInterval = dateArray.get(1).size();
		if(noOfInterval ==1){
			noOfInterval = 2;
		}
	}
	public void getData() {
		// TODO Auto-generated method stub
		this.countArray = DBAider.countArray;
		this.dateArray = DBAider.dateArray;
		//System.out.println("in get data dateArray:"+dateArray);
		//System.out.println("in get data countArray:"+countArray);
		noOfInterval = dateArray.get(0).size();
		noOfKeywords = 2;
//		if(db.connect()){
//			db.query("select count(*) from temp where keywords MATCH 'fire'");
//			while(db.next()){
//				String id = db.getString("datetime");
//				//System.out.println(id);
//			}
//		}
//		db.close();
	}


	private void drawAxisLabels() {
		// TODO Auto-generated method stub
		drawYAxisLabels();
		drawXAxisLabels();
	}



	private void drawXAxisLabels() {
		// TODO Auto-generated method stub
		float interval = (graphWidth-3*graphXBuffer)/(noOfInterval-1);
		float intervalLoop = 0;
		for(int i = 0 ;i<noOfInterval; i++){
			line(2*graphXBuffer+intervalLoop,graphHeight-2*graphYBuffer,2*graphXBuffer+intervalLoop,graphHeight-2*graphYBuffer+4);
			fill(EnumColor.BLACK);
			textAlign(PConstants.CENTER, PConstants.BOTTOM);
			//pushMatrix();
			//Main.main.translate(2*graphXBuffer+intervalLoop,graphHeight-2*graphYBuffer+20);
			//Main.main.rotate(-(PConstants.HALF_PI));
			if (drawGraph1){
			text(String.valueOf(dateArray.get(0).get(i)),2*graphXBuffer+intervalLoop,graphHeight-2*graphYBuffer+20);
			}
			else if(drawGraph2){
				text(String.valueOf(dateArray.get(1).get(i)),2*graphXBuffer+intervalLoop,graphHeight-2*graphYBuffer+20);
				
			}
			//popMatrix();
			intervalLoop += interval;
		}
	}



	private void drawYAxisLabels() {
		// TODO Auto-generated method stub
		
		float interval = (graphHeight-3*graphYBuffer)/10;
		float intervalLoop = 0;
		float countInterval = countMax/10;
		float countIntervalLoop = 0;
		if(countMax < 10)
		{
			interval = (graphHeight-3*graphYBuffer)/countMax;
			countInterval = 1;
		}
		for(int i = 0; i < 11; i++){
			line(2*graphXBuffer,graphHeight-2*graphYBuffer-intervalLoop,2*graphXBuffer-4,graphHeight-2*graphYBuffer-intervalLoop);
			fill(EnumColor.BLACK);
			textAlign(PConstants.CENTER, PConstants.CENTER);
			text(String.valueOf(Math.round(countIntervalLoop)),2*graphXBuffer-20,graphHeight-2*graphYBuffer-intervalLoop);
			countIntervalLoop += countInterval;
			intervalLoop += interval;
		}
		
	}



	private void drawAxis() {
		// TODO Auto-generated method stub	
		//stroke(EnumColor.WHITE);
		//X-axis
		line(graphXBuffer,graphHeight-2*graphYBuffer,graphWidth-graphXBuffer,graphHeight-2*graphYBuffer);
		fill(EnumColor.BLACK);
		textAlign(PConstants.CENTER, PConstants.CENTER);
		text("Day of Month(30thApril to 20th May)",graphWidth/2,graphHeight-graphYBuffer/2);
		//Y-axis
		line(2*graphXBuffer,graphYBuffer,2*graphXBuffer,graphHeight-graphYBuffer);
		textAlign(PConstants.CENTER, PConstants.CENTER);
		text("Count",20,graphHeight/2);
		
	}
	
	

}
