package edu.cs424.p4.panels;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.MainPanel;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.PubSub.Event;
import static edu.cs424.p4.central.AppConstants.*;

public class InfoPanel extends Panel implements TouchEnabled
{

	Button button[];
	GraphInfoPanel graphInfoPanel1,graphInfoPanel2;
	public static int selectedButton = 0;
	
	public InfoPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) {
		super(x0, y0, width, height, parentX0, parentY0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		if ( event == MouseMovements.MOUSEDOWN)
		{
			int counter = 0;
			
			for(Button but : button)
			{
				if(but.containsPoint(x, y))
				{
					if( !but.isPressed )
					{
						but.setPressed(true);
						selectedButton = counter;
						
						if (counter == 0)
						{
							PubSub.publishEvent(Event.FOCUS_GRAPH1,null);
							MainPanel.graphPanel.getData1();
						}
						else
						{
							PubSub.publishEvent(Event.FOCUS_GRAPH2,null);
							MainPanel.graphPanel.getData2();
						}
							
						break;
					}
				}
				counter++;
			}
			
			counter = 0;
			for( Button but : button)
			{
				if(selectedButton != counter)
					but.setPressed(false);
				
				counter++;
			}			
			
			
			if( selectedButton == 0 && graphInfoPanel1.containsPoint(x, y) )
			{
				graphInfoPanel1.touch(x, y, event);
			}
			else if( selectedButton == 1 && graphInfoPanel2.containsPoint(x, y))
			{
				graphInfoPanel2.touch(x, y, event);				
			}
			
		}
		return false;
	}

	@Override
	public void setup() {
		button = new Button[2];
		String[] name = {"Graph 1","Graph 2"};

		for(int i = 0 ; i < 2 ; i++ )
		{
			button[i] = new Button(5 + (111*i) ,167 , 106 , 15 , x0, y0, name[i], true);
			button[i].setup();
			if( i == 0 )
			{
				button[i].setPressed(true);
				button[i].setSelectedTextColor(EnumColor.GRAPH1);
			}
			else
				button[i].setSelectedTextColor(EnumColor.GRAPH2);
		}
		
		graphInfoPanel1 = new GraphInfoPanel(infoInnerPanelX, infoInnerPanelY, infoInnerPanelWidth, infoInnerPanelHeight, x0, y0, Event.UPDATE_GRAPH1);
		graphInfoPanel1.setup();
		
		graphInfoPanel2 = new GraphInfoPanel(infoInnerPanelX, infoInnerPanelY, infoInnerPanelWidth, infoInnerPanelHeight, x0, y0, Event.UPDATE_GRAPH2);
		graphInfoPanel2.setup();
		
	}
	
	@Override
	public void draw()
	{
		background(EnumColor.BLACK);
		for(int i = 0 ; i < 2 ; i++ )
		{
			button[i].draw();
		}
		
		if( selectedButton == 0)
		{
			graphInfoPanel1.draw();
		}
		else if( selectedButton == 1)
		{
			graphInfoPanel2.draw();
		}
	}

}
