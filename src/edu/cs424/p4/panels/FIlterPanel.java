package edu.cs424.p4.panels;

import java.util.ArrayList;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.ListPanel;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.PubSub.Event;
import edu.cs424.p4.pubsub.Subscribe;
import static edu.cs424.p4.central.AppConstants.*;

public class FIlterPanel extends Panel implements TouchEnabled,Subscribe
{
	Button button[];
	int selectedButton = 0; // 0 = keyword , 1 = people , 3 = search , 4= time of day , 5 = load query
	
	GraphFilterPanel graph1,graph2;
	Event selectedGraph;

	public FIlterPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0)
	{
		super(x0, y0, width, height, parentX0, parentY0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) 
	{
		if ( event == MouseMovements.MOUSEDOWN)
		{
			int counter = 0;
			
			for(Button but : button)
			{
				if(but.containsPoint(x, y))
				{
					if( !but.isPressed )
					{
						but.setPressed(true);
						selectedButton = counter;
						break;
					}
				}
				counter++;
			}
			
			counter = 0;
			for( Button but : button)
			{
				if(selectedButton != counter)
					but.setPressed(false);
				
				counter++;
			}				
			
			if(selectedGraph == Event.FOCUS_GRAPH1 && graph1.containsPoint(x, y) )
			{
				graph1.touch(x, y, event);
			}
			else if( selectedGraph == Event.FOCUS_GRAPH2 && graph2.containsPoint(x, y) )
			{
				graph2.touch(x, y, event);				
			}
			
		}
		return false;
	}

	@Override
	public void setup() 
	{
		PubSub.subscribeEvent(Event.FOCUS_GRAPH1, this);
		PubSub.subscribeEvent(Event.FOCUS_GRAPH2,this);
		selectedGraph = Event.FOCUS_GRAPH1;
		
		button = new Button[5];
		String[] name = {"Keyword","People","Search","Time","Query"};

		for(int i = 0 ; i < 5 ; i++ )
		{
			button[i] = new Button(40 + (49*i) ,163 , 45 , 20 , x0, y0, name[i], true);
			button[i].setup();
			if( selectedButton == i )
				button[i].setPressed(true);
		}
		
		button[4].setVisibilty(false);
		graph1 = new GraphFilterPanel(0, 0, filterPanelWidth, 163, x0, y0, Event.FOCUS_GRAPH1);
		graph1.setup();

		graph2 = new GraphFilterPanel(0, 0, filterPanelWidth, 163, x0, y0, Event.FOCUS_GRAPH2);
		graph2.setup();

	}

	@Override
	public void draw() 
	{
		background(EnumColor.BLACK);
		
		for(int i = 0 ; i < 5 ; i++ )
		{
			button[i].draw();
		}
		
		if(selectedGraph == Event.FOCUS_GRAPH1)
		{
			graph1.draw(selectedButton);
		}
		else
		{
			graph2.draw(selectedButton);
		}
		
	}

	@Override
	public void receiveNotification(Event eventName, Object... object)
	{
		if(eventName == Event.FOCUS_GRAPH1)
		{
			selectedGraph = Event.FOCUS_GRAPH1;
		}
		else if(eventName == Event.FOCUS_GRAPH2)
		{
			selectedGraph = Event.FOCUS_GRAPH2;
		}
	
		
	}

}
