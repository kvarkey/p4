package edu.cs424.p4.panels;


import processing.core.PConstants;
import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;

public class EventPanel extends Panel implements TouchEnabled
{

	public EventPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) 
	{
		super(x0, y0, width, height, parentX0, parentY0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setup() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void draw() 
	{
		background(EnumColor.GRAY);
		pushStyle();
		
		textAlign(PConstants.LEFT, PConstants.CENTER);
		textSize(12);
		fill(EnumColor.WHITE);
		text("Events", 5, 5);
		
		textSize(10);
		
		text("Keywords : ", 5, 30 +(40 *0) , 300, 40);
		text("bbbbbbbbbbbbbbbbbbbbbbbb", 5, 30 +(40*1) , 300, 40);
		text("cccccccccccccccccccccccccccc", 5, 30 +(40*2) , 300, 40);
		text("dddddddddddddddddddddddddddddddddddddddddddddddd", 5, 30 +(40*3) , 300, 40);
		
		popStyle();
	}

}
