package edu.cs424.p4.panels;

import processing.core.PConstants;
import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.Keyboard;
import edu.cs424.p4.central.components.SmallKeyboard;
import edu.cs424.p4.db.DBAider;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.PubSub.Event;
import edu.cs424.p4.pubsub.Subscribe;
import static edu.cs424.p4.central.AppConstants.*;

public class FloatingKeyboard extends Panel implements TouchEnabled,Subscribe
{
	Event currentEvent;

	SmallKeyboard keyboard;
	Button ok,cancel;
	
	String oldData,newData;

	public FloatingKeyboard(float x0, float y0, float width, float height,
			float parentX0, float parentY0) 
	{
		super(x0, y0, width, height, parentX0, parentY0);

	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {

		if( event == MouseMovements.MOUSEDOWN)
		{
			if(keyboard.containsPoint(x, y))
				keyboard.touch(x, y, event);
			
			else if ( ok.containsPoint(x, y) )
			{
				if( currentEvent == Event.SAVE_USER )
				{
					DBAider.saveNewUser(oldData, keyboard.getText());
					PubSub.publishEvent(Event.CLOSE_FLOATING_WINDOW, null);
					PubSub.publishEvent(Event.NEW_USER_ADDED,null);
				}
			}
			else if ( cancel.containsPoint(x, y) )
			{
				
				PubSub.publishEvent(Event.CLOSE_FLOATING_WINDOW, null);
			}
				

		}

		return false;
	}

	@Override
	public void setup()
	{
		PubSub.subscribeEvent(Event.SAVE_USER, this);

		keyboard = new SmallKeyboard(infoPanelX, infoPanelY,250, 105, x0,y0);
		keyboard.setup();

		ok = new Button(infoPanelX,infoPanelY + 110,40,20,x0,y0,"Save",true);
		ok.setup();

		cancel = new Button(infoPanelX+ 50 ,infoPanelY + 110,40,20,x0,y0,"cancel",true);
		cancel.setup();


	}

	@Override
	public void draw() 
	{
		pushStyle();
		background(EnumColor.BLACK, 200);
		keyboard.draw();
		fill(EnumColor.WHITE);
		textSize(12);
		textAlign(PConstants.LEFT, PConstants.CENTER);
		text("Save New User", width/2 ,  height/2 - 20);

		ok.draw();
		cancel.draw();
		popStyle();
	}

	@Override
	public void receiveNotification(Event eventName, Object... object)
	{
		if ( eventName == Event.SAVE_USER )
		{
			currentEvent = eventName;
			oldData = ((String)object[0]).split(" : ")[0];
		}
		else
		{
			System.out.println("FloatingKeyboard.receiveNotification()" + "event undefined " + eventName );
		}

	}


}
