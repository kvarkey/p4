package edu.cs424.p4.panels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PVector;
import static edu.cs424.p4.central.AppConstants.*;
import edu.cs424.p4.central.AppConstants;
import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.EnumConfig;
import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.MainPanel;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.SettingsLoader;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.Slider;
import edu.cs424.p4.central.components.Touch;
import edu.cs424.p4.central.components.map.Location;
import edu.cs424.p4.central.components.map.Marker;
import edu.cs424.p4.central.components.map.MercatorMap;
import edu.cs424.p4.central.components.map.SimplePointMarker;
import edu.cs424.p4.db.DB;
import edu.cs424.p4.db.DBAider;
import edu.cs424.p4.db.Tweet;

public class NewMapPanel extends Panel implements TouchEnabled
{
	//Stuff related to touch
	PVector lastTouchPos;
	PVector lastTouchPos2;
	PVector initTouchPos,initTouchPos2;
	int touchID1;
	int touchID2;
	Hashtable touchList;
	List<Marker> markers;
	public static List<Location> locationsToPlot;
	public static boolean G1_SHOW=true,G2_SHOW=true;
	//End Touch
	
	MercatorMap mmap;
	
	PImage mapImage;

	//all required coordinates and step values
	float translateY = 10, translateX = 30;

	float imageCenterX, imageCenterY, zoomHeight, zoomWidth, currentCenterX, currentCenterY;
	public float currentTopLeftX = mapX;
	float currentBottomRightX = mapWidth;
	public float currentTopLeftY = mapY;
	float currentBottomRightY = mapHeight;
	

	int zoomLevel = 1, noMoreZoomIn = 5, noMoreZoomOut = 1;
	Location[] boundaryLocations = new Location[2];
	
	//added by kush
	PFont font;
	PImage one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,
		twenty,thirty;
	//

	//Buttons
	ArrayList<Button> mapButtons = new ArrayList<Button>();
	Button zoomIn, zoomOut, panLeft, panUp, panRight, panDown;

	public NewMapPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) {
		super(x0, y0, width, height, parentX0, parentY0);
		//Markers
		markers = new ArrayList<Marker>();
		locationsToPlot = new ArrayList<Location>();
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		System.out.println("MapPanel.touch() - Not using anymore.");
		return false;
	}

	public boolean touch(int ID,float x, float y, MouseMovements event) {
		System.out.println("MapPanel.touch()" );

		if(this.containsPoint(x, y))
		{
			if(MouseMovements.MOUSEDOWN == event)
			{
				
				
				//if xy is on the buttons
				if(isOnButton(x, y))
				{
					
					return false;
				}

				//else xy is on the active part of map
				else if (x > s(mapX) && x < s(mapWidth) 
						&& y > s(mapY) && y < s(mapHeight)) {
					//=======Stuff related to touch
//					mapMouseDown(ID,x,y);
					//=========Touch end
					System.out.println("Calling updatemarkers");
					updateMarkers(x,y);
				}
				
			}
			
		}

		return false;
	}


	@Override
	public void setup() {
		this.mapImage = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.MAPIMAGE));
		System.out.println("print"+SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.MAPIMAGE));
		//added by kush
		this.one = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.ONE));
		System.out.println("print"+SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.ONE));
		this.two = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.TWO));
		this.three = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.THREE));
		this.four = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.FOUR));
		this.five = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.FIVE));
		this.six = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.SIX));
		this.seven = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.SEVEN));
		this.eight = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.EIGHT));
		this.nine = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.NINE));
		this.ten = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.TEN));
		this.eleven = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.ELEVEN));
		this.twelve = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.TWELVE));
		this.thirteen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.THIRTEEN));
		this.fourteen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.FOURTEEN));
		this.fifteen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.FIFTEEN));
		this.sixteen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.SIXTEEN));
		this.seventeen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.SEVENTEEN));
		this.eighteen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.EIGHTEEN));
		this.nineteen = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.NINETEEN));
		this.twenty = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.TWENTY));
		this.thirty = loadImage(SettingsLoader.dir + SettingsLoader.getConfigValueAsString(EnumConfig.THIRTY));
		//
		
		this.imageCenterX = mapWidth/2;
		this.imageCenterY = mapHeight/2;
		this.zoomWidth = mapWidth;
		this.zoomHeight = mapHeight;
		initButtons();
		
		touchList = new Hashtable();
		mmap = new MercatorMap(0,0,zoomWidth, zoomHeight, 42.3017f, 42.1609f, 93.5673f, 93.1923f);
		
		addMarker();
		
		//added by kush
		font = SettingsLoader.getClimacon();
		//
		
		DBAider.initData();
	}

	@Override
	public void draw()
	{
		pushStyle();
		background(EnumColor.BLACK);
		imageMode(PApplet.CENTER);
		image(mapImage, imageCenterX, imageCenterY, zoomWidth, zoomHeight);

		

		//get data from db
		getData();
		
		//Update the map if zoomed or panned
		updateMercatorMap();
		
		//draw markers on the map
		drawMarkers();
		
		for(Button b:mapButtons)
			b.draw();
		//Main.main.fill(24);
		rect(x0+width,y0,Main.main.getWidth(),Main.main.getHeight());
		popStyle();
		if(MainPanel.dayButton!=null && MainPanel.dayButton.isPressed){
			drawClimate();
		}
	}
	
	public void drawClimate(){
		pushStyle();
		textFont(font);
		textSize(48);
		if(Slider.currentMinDate.getDayOfMonth() == 1){
			image(one,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 2){
			image(two,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 3){
			image(three,mapWidth-80,0,50,50);
			text("9",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 4){
			image(four,mapWidth-80,0,50,50);
			text("9",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 5){
			image(five,mapWidth-80,0,50,50);
			text("3",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 6){
			image(six,mapWidth-80,0,50,50);
			text("3",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 7){
			image(seven,mapWidth-80,0,50,50);
			text("3",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 8){
			image(eight,mapWidth-80,0,50,50);
			text("`",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 9){
			image(nine,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 10){
			image(ten,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 11){
			image(eleven,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 12){
			image(twelve,mapWidth-80,0,50,50);
			text("`",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 13){
			image(thirteen,mapWidth-80,0,50,50);
			text("3",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 14){
			image(fourteen,mapWidth-80,0,50,50);
			text("`",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 15){
			image(fifteen,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 16){
			image(sixteen,mapWidth-80,0,50,50);
			text("`",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 17){
			image(seventeen,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 18){
			image(eighteen,mapWidth-80,0,50,50);
			text("v",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 19){
			image(nineteen,mapWidth-80,0,50,50);
			text("`",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 20){
			image(twenty,mapWidth-80,0,50,50);
			text("`",mapWidth-10,50);}
		else if(Slider.currentMinDate.getDayOfMonth() == 30){
				image(thirty,mapWidth-80,0,50,50);
				text("`",mapWidth-10,50);}
		popStyle();
	}
	
	public void updateMercatorMap()
	{
		if(mmap.mapScreenWidth!=zoomWidth || mmap.mapScreenHeight!=zoomHeight)
		{
			mmap = new MercatorMap(currentTopLeftX,currentTopLeftY,zoomWidth, zoomHeight, 42.3017f, 42.1609f, 93.5673f, 93.1923f);
			addMarker();
		}
		
	}
	
	public void addMarker()
	{
		markers = new ArrayList<Marker>();
		int prevcount;
		if(DBAider.userMap!=null && DBAider.userMap.size()>0 )
		{
			 
			drawMarkerForGraph(DBAider.userMap,AppConstants.GRAPH1_FILL_COLOR,AppConstants.GRAPH1_HIGHLIGHTED_FILL_COLOR, "G1");
			
		}
		if(DBAider.userMap2!=null && DBAider.userMap2.size()>0 )
		{
			
				drawMarkerForGraph(DBAider.userMap2,AppConstants.GRAPH2_FILL_COLOR,AppConstants.GRAPH2_HIGHLIGHTED_FILL_COLOR, "G2");
			
		}
		//Location l = new Location(42.2183f,93.38957f);		
	}
	
	public void drawMarkerForGraph(HashMap<Integer,ArrayList<Tweet>> map, int color, int highlightcolor, String g)
	{
		for(int id : map.keySet())
		{
			
			ArrayList<Tweet> tweetlist = map.get(id);
			for(Tweet t : tweetlist){
				Location l = new Location(t.lat,t.lon);
				SimplePointMarker m = new SimplePointMarker(l);
				m.setColor(color);
				m.setHighlightColor(highlightcolor);
				m.setStrokeColor(255);
				HashMap<String, Object> hm = new HashMap<String, Object>();
				hm.put("Tweet", t.tweet);
				hm.put("Date Time", new DateTime( t.datetime));
				hm.put("User ID", t.userid);
				hm.put("xy", toMapPoint(l));
				hm.put("graph", g);
				m.setProperties(hm);
				markers.add(m);
			}
		}
		//Location l = new Location(42.2183f,93.38957f);		
	}

	
	public void drawMarkers()
	{
		for(Marker newMarker : markers)
		{
			SimplePointMarker m = (SimplePointMarker) newMarker;
			String g =(String) m.getProperties().get("graph");
			if(g.equalsIgnoreCase("G1"))
			{
				if(G1_SHOW)
				{
					newMarker.draw(this);
				}
			}
			else
			{
				if(G2_SHOW)
					newMarker.draw(this);
			}
			
		}
	}
	
	//FIXME Works only with first zoom level.
	public void updateMarkers(float mx, float my)
	{
		
		for(Marker newMarker : markers)
		{
			
			if(newMarker.isInside(this, mx, my))
			{
				SimplePointMarker m = (SimplePointMarker)newMarker;
				newMarker.setSelected(true);
				drawDetailsBox(m,(PVector)newMarker.getProperties().get("xy"),m.getRadius());
			}
			else
				newMarker.setSelected(false);
		}
	}
	
	public void highlightMarker(int id)
	{
		for(Marker newMarker : markers)
		{
			SimplePointMarker m = (SimplePointMarker) newMarker;
			if((Integer)m.getProperties().get("User ID")==id)
			{
				m.setSelected(true);
			}
			else
			{
				m.setSelected(false);
			}
		}
	}

	public void getData() {
		//get data from db
		
	}

	public Location[] getBoundaryLatLong() {
		return boundaryLocations;
	}

	void updateVisibleCoordinates() {
		updateCenter();

		currentTopLeftX = imageCenterX - zoomWidth/2; currentBottomRightX = imageCenterX + zoomWidth/2;;
		currentTopLeftY = imageCenterY - zoomHeight/2; currentBottomRightY = imageCenterY + zoomWidth/2;

		float currentTopLeftLong = PApplet.map(currentTopLeftX, 0, zoomWidth, topLeftLon, bottomRightLon);
		float currentTopLeftLat = PApplet.map(currentTopLeftY, 0, zoomHeight, topLeftLat, bottomRightLat);
		boundaryLocations[0] = new Location(currentTopLeftLat, currentTopLeftLong);

		float currentBottomRightLong = PApplet.map(currentBottomRightX, 0, zoomWidth, topLeftLon, bottomRightLon);
		float currentBottomRightLat = PApplet.map(currentBottomRightY, 0, zoomHeight, topLeftLat, bottomRightLat);
		boundaryLocations[1] = new Location(currentBottomRightLat, currentBottomRightLong);

	}

	void updateCenter() {
		//if new left edge is to right of visible-area-left-edge, align image by left edge
		if(imageCenterX - zoomWidth/2 > x0)
			imageCenterX = zoomWidth/2;

		//elsewhere, if new right edge is to left of visible-area-right-edge, align image by right
		else if(imageCenterX + zoomWidth/2 < mapWidth)
			imageCenterX = mapWidth - zoomWidth/2;

		if(imageCenterY - zoomHeight/2 > y0)
			imageCenterY = zoomHeight/2;

		else if(imageCenterY + zoomHeight/2 < mapHeight)
			imageCenterY = mapHeight - zoomHeight/2;
	}

	void initButtons() {
		panUp = new Button(zoomInButtonX, zoomInButtonY-zoomButtonHeight-3,
				zoomButtonWidth, zoomButtonHeight, x0, y0, "^", true);

		zoomIn = new Button(zoomInButtonX, zoomInButtonY, 
				zoomButtonWidth, zoomButtonHeight, x0, y0, "+", true);

		zoomOut = new Button(zoomInButtonX, zoomInButtonY+zoomButtonHeight, 
				zoomButtonWidth, zoomButtonHeight, x0, y0, "-", true);

		panDown = new Button(zoomInButtonX, zoomInButtonY+zoomButtonHeight*2+3,
				zoomButtonWidth, zoomButtonHeight, x0, y0, "v", true);

		panLeft = new Button(panLeftButtonX, panLeftButtonY,
				zoomButtonWidth, zoomButtonHeight, x0, y0, "<", true);

		panRight = new Button(panRightButtonX, panRightButtonY,
				zoomButtonWidth, zoomButtonHeight, x0, y0, ">", true);

		mapButtons.add(zoomIn);
		mapButtons.add(zoomOut);
		mapButtons.add(panLeft);
		mapButtons.add(panUp);
		mapButtons.add(panRight);
		mapButtons.add(panDown);
	}

	public void zoomIn() {
		if(zoomLevel < noMoreZoomIn) {
			zoomWidth *= 1.5f;
			zoomHeight *= 1.5f;

			zoomLevel++;
			updateVisibleCoordinates();
		}
	}

	public void zoomOut() {
		if(zoomLevel > noMoreZoomOut) {
			zoomWidth /= 1.5f;
			zoomHeight /= 1.5f;

			zoomLevel--;
			updateVisibleCoordinates();
		}
	}

	public void panUp() {
		if(zoomLevel != 1)
			imageCenterY += translateY;

		updateVisibleCoordinates();
	}

	public void panDown() {
		if(zoomLevel != 1)
			imageCenterY -= translateY;

		updateVisibleCoordinates();
	}

	public void panLeft() {
		if(zoomLevel != 1)
			imageCenterX += translateX;

		updateVisibleCoordinates();
	}

	public void panRight() {
		if(zoomLevel != 1)
			imageCenterX -= translateX;

		updateVisibleCoordinates();
	}

	boolean isOnButton(float x, float y) {
		
		if(zoomIn.containsPoint(x, y)) {
			zoomIn();
			updateVisibleCoordinates();
			return true;
		}
		else if(zoomOut.containsPoint(x, y)) {
			zoomOut();
			updateVisibleCoordinates();
			return true;
		}
		else if(panUp.containsPoint(x, y)) {
			panUp();
			updateVisibleCoordinates();
			return true;
		}
		else if(panDown.containsPoint(x, y)) {
			panDown();
			updateVisibleCoordinates();
			return true;
		}
		else if(panLeft.containsPoint(x, y)) {
			panLeft();
			updateVisibleCoordinates();
			return true;
		}
		else if(panRight.containsPoint(x, y)) {
			panRight();
			updateVisibleCoordinates();
			return true;
		}
		return false;
	}

	
	//************Touch related stuff
	private void mapMouseDown(int ID, float x, float y) {
		// TODO Auto-generated method stub
		// Update the last touch position
		lastTouchPos = createPvector(x, y);
		
		// Add a new touch ID to the list
		  Touch t = new Touch( ID, x, y);
		  touchList.put(ID,t);
		  
		  if( touchList.size() == 1 ){ // If one touch record initial position (for dragging). Saving ID 1 for later
			    touchID1 = ID;
			    initTouchPos = createPvector(x, y);
			  }
			  else if( touchList.size() == 2 ){ // If second touch record initial position (for zooming). Saving ID 2 for later
			    touchID2 = ID;
			    initTouchPos2 = createPvector(x, y);
			  }
	}
	
	private void mapMouseMove(int ID, float x, float y)
	{
		if( touchList.size() < 2 ){
//		       Only one touch, drag map based on last position
//		      this.tx += (x - lastTouchPos.x)/this.sc;
//		      this.ty += (y - lastTouchPos.y)/this.sc;
			float dx = (x - lastTouchPos.x)/this.zoomLevel;
			float dy = (y - lastTouchPos.y)/this.zoomLevel;
			if(zoomLevel != 1){
				imageCenterX += dx;
				imageCenterY -= dy;
			}

			updateVisibleCoordinates();
	    }
	}
	//************Touch end
	
	public PVector toMapPoint(Location p)
	{
		return mmap.getScreenLocation(new PVector(p.lat,p.lon));
	}
	
	public void drawDetailsBox(SimplePointMarker m, PVector xy,float r)
	{
		float boxw = r*4;
		float boxh = r*3;
		pushStyle();
		textSize(6);
		fill(255);
		stroke(0);
		//text(m.getProperties().get("Tweet"),)
		//rect()
		popStyle();
	}

}
