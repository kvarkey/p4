package edu.cs424.p4.central;

import edu.cs424.p4.central.TouchListener.MouseMovements;




public interface TouchEnabled {
	
	  public boolean touch(float x, float y, MouseMovements event);

	  public boolean containsPoint(float x, float y);

}
