package edu.cs424.p4.central.components.map;

import processing.core.*;

public class Location extends PVector {

  public float lat;
  public float lon;

  public Location(float lat, float lon) {
    this.lat = lat;
    this.lon = lon;
  }

  public String toString() {
    return "(" + PApplet.nf(lat,1,3) + ", " + PApplet.nf(lon,1,3) + ")";
  }
  
  public float getLat()
  {
	  return lat;
  }
  
  public float getLon()
  {
	  return lon;
  }  

}
