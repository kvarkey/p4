package edu.cs424.p4.central.components.map;



import java.util.HashMap;

import edu.cs424.p4.central.EnumConfig;
import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.SettingsLoader;
import edu.cs424.p4.panels.NewMapPanel;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;


/**
 * Simple marker representing a single location as a circle.
 */
public class SimplePointMarker extends AbstractMarker {

	protected float radius = 10f;
	float radius2 = 20f;

	/**
	 * Creates an empty point marker. Used internally by the MarkerFactory.
	 */
	public SimplePointMarker() {
		this(null, null);
	}

	/**
	 * Creates a point marker for the given location.
	 * 
	 * @param location
	 *            The location of this Marker.
	 */
	public SimplePointMarker(Location location) {
		this(location, null);
	}

	/**
	 * Creates a point marker for the given location and properties.
	 * 
	 * @param location
	 *            The location of this Marker.
	 * @param properties
	 *            Some data properties for this marker.
	 */
	public SimplePointMarker(Location location, HashMap<String, Object> properties) {
		super(location, properties);
	}

	/**
	 * Draws this point marker as circle in the defined style. If no style has been set, Unfolding's default one is
	 * used.
	 */
	@Override
	public void draw (NewMapPanel pg,float x, float y) {
		pg.pushStyle();
		pg.strokeWeight(strokeWeight);
		if (isSelected()) {
			pg.fill(highlightColor);
			pg.stroke(highlightStrokeColor);
			pg.ellipse((int)x, (int)y, radius2, radius2);
		} else {
			pg.fill(color,120);
			pg.stroke(strokeColor);
			pg.ellipse((int)x, (int)y, radius, radius);
		}
		 // TODO use radius in km and convert to px
		pg.popStyle();
	}

	@Override
	public boolean isInside(float checkX, float checkY, float x, float y) {
		PVector pos = new PVector(x, y);
		float d = pos.dist(new PVector(checkX, checkY));
		//System.out.println("Checking if ("+checkX+","+checkY+")" +" is within "+"("+x+","+y+")"  );
		float disX = x - checkX;
		float disY = y - checkY;
		//System.out.println("disx:"+disX);
		//System.out.println("disx:"+disY);
		float v = Main.main.sqrt(Main.main.sq(disX) + Main.main.sq(disY));
		System.out.println("Dist: "+v+"<"+radius);
		return (v < radius);
		//return pos.dist(new PVector(checkX, checkY)) < radius; // FIXME must be zoom dependent
	}

	/**
	 * Sets the radius of this marker. Used for the displayed ellipse.
	 * 
	 * @param radius
	 */
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public float getRadius()
	{
		return this.radius;
	}
	
	

}