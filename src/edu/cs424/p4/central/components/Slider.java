package edu.cs424.p4.central.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;

import edu.cs424.p4.central.AppConstants;
import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.MainPanel;
import edu.cs424.p4.central.Panel;

/**
 * Reusable Slider component.
 * Color values are hard coded.
 * @author GV
 *
 */
public class Slider
{
  float x,y,x2,y2;
  float mmin,mmax;
  Integer yearMin,yearMax;
  public static DateTime currentMinDate,currentMaxDate;
  List<DateTime> dateList;
  HashMap<Integer,Touch> touchIDs;
  //public static Button dayButton;
  public Slider(float x, float y, float x2, float y2)
  {
    this.x = x;
    this.y = y;
    this.x2 = x2;
    this.y2 = y2;
    dateList = getDates();
    currentMinDate = dateList.get(0);
    currentMaxDate = dateList.get(dateList.size()-1);
    touchIDs = new HashMap<Integer, Touch>();
  }
  
  public void drawSlider()
  {

	Main.main.pushStyle();
	Main.main.textSize(Panel.s(6));
    Main.main.rectMode(Main.main.CORNERS);
    Main.main.strokeWeight(Panel.s(3));
    //Main.main.rect(x,y,x2,y2);
    //Main.main.fill(24,200);
    int totalDays = dateList.size();
    
    DateTime currentDay = dateList.get(0);
    
    //draws lines for slider
      int dcount = 0;
      
      if(MainPanel.dayButton.isPressed)
    	  currentMinDate = currentMaxDate;
      else if(currentMinDate.compareTo(currentMaxDate)==0)
    	  currentMinDate = currentMaxDate.minusDays(1);
      
      Main.main.pushStyle();
      //draws min ellipse
      Main.main.fill(0,200);
      Main.main.stroke(0);
      int currentMinPos = resolvePosition(currentMinDate);
      mmin = Main.main.map(currentMinPos,0, totalDays, x, x2);
      //Main.main.noFill();
      Main.main.ellipse(mmin,(y+y2)/2,y2*0.025f,y2*0.025f);
      
    //draws max ellipse
      Main.main.stroke(255);
      Main.main.fill(255,200);
      int currentMaxPos = resolvePosition(currentMaxDate);
      mmax = Main.main.map(currentMaxPos,0, totalDays, x, x2);
      Main.main.noFill();
      Main.main.ellipse(mmax,(y+y2)/2,y2*0.025f,y2*0.025f);
      Main.main.popStyle();
      
      Main.main.line(getPositionYearMin(),(y+y2)/2,getPositionYearMax(),(y+y2)/2);
      
    //draws ticks on the slider.
    for(;dateCompare(currentDay,dateList.get(dateList.size()-1))<1;currentDay.plusDays(1)){
      float m = Main.main.map(dcount, 0, totalDays, x,x2);
    	  Main.main.stroke(255,200);
        float r = y2-y;
        Main.main.line(m,y+r*0.4f,m,y2-r*0.4f);
        Main.main.fill(0);
        Main.main.textAlign(Main.main.CENTER,Main.main.TOP);
        Main.main.text(currentDay.getDayOfMonth(),m,y2-r*0.4f);
        String pDow = currentDay.dayOfWeek().getAsShortText().substring(0, 1);
        Main.main.text(pDow,m,y2-r*0.4f+Main.main.textAscent()+Main.main.textDescent());
        currentDay = currentDay.plusDays(1);
      dcount++;
    }
    
    Main.main.stroke(243,171,9,50);
    Main.main.line(x,(y+y2)/2,x2,(y+y2)/2); //center line of the range slider.
    
    Main.main.stroke(243,171,9);
    Main.main.line(getPositionYearMin(),(y+y2)/2,getPositionYearMax(),(y+y2)/2);
    
    Main.main.popStyle();
  }
  
  public void updateSlider(int ID, float mx, float my){
	  if(currentMinDate.compareTo(currentMaxDate )<=0)
	         updatePosition(ID,mx,my);
  }
  
  
  boolean isWithinSlider(float mx, float my)
  {
    if (mx >= x && mx <= x2 && 
      my >= y && my <= y2) {
      return true;
    }
    else return false;
  }
  
  void updatePosition(int ID, float mx, float my)
  {
	if(!MainPanel.dayButton.isPressed)
	{
		if(currentMaxDate.compareTo(currentMinDate)==0)
			currentMinDate = currentMaxDate.minusDays(1);
	    float disMinX = (mx > mmin)? mx-mmin: mmin-mx;
	    float disMaxX = (mx > mmax)? mx-mmax: mmax-mx;
	    System.out.println("==");
	    System.out.println("disMinX:"+disMinX);
	    System.out.println("disMaxX:"+disMaxX);
	    if(disMinX < disMaxX)
	    {
	       int v = getDatePosition(mx); 
	       DateTime t = dateList.get(v);
	       if(t.compareTo(currentMaxDate)<0)
	         currentMinDate = t;
	    }
	    else if(disMinX > disMaxX)
	    {
	      int v = getDatePosition(mx); 
	      DateTime t = dateList.get(v);
	      if(t.compareTo(currentMinDate)>0)
	        currentMaxDate = t;
	    }
	}
    else
    {
    	System.out.println("sssss");
    	int v1 = getDatePosition(mx);
    	DateTime t = dateList.get(v1);
    	currentMinDate = t;
    	currentMaxDate = t;
    }
  }
  
  int getDatePosition(float inx)
  {
    return (int)Main.main.map(inx,x,x2,0,dateList.size());
  }
  
  float getPositionYearMin()
  {
    return Main.main.map(resolvePosition(currentMinDate),0,dateList.size(),x,x2);
  }
  
  float getPositionYearMax()
  {
    return Main.main.map(resolvePosition(currentMaxDate),0,dateList.size(),x,x2);
  }
  
  static List<DateTime> getDates()
  {
	  DateTime startDate,endDate;
      startDate = new DateTime("2011-04-30");
      endDate = new DateTime("2011-05-21");
	  List<DateTime> dates = new ArrayList<DateTime>();
	  int days = Days.daysBetween(startDate, endDate).getDays();
	  for (int i=0; i < days; i++) {
	      DateTime d = startDate.withFieldAdded(DurationFieldType.days(), i);
	      dates.add(d);
	  }
	  return dates;
  }
  
  static int dateCompare(DateTime first, DateTime second)
  {
	  LocalDate firstDate = first.toLocalDate();
	  LocalDate secondDate = second.toLocalDate();
	  return firstDate.compareTo(secondDate);
  }
  
  int resolvePosition(DateTime posD)
  {
	  List<DateTime> dList = dateList;
	  int count = 0;
	  for(DateTime dt : dList)
	  {
		  if(posD.compareTo(dt)==0)
			  break;
		  count++;
	  }
	  return count;
  }
  
  public static void main(String k[])
  {
	  
	  List<DateTime> dateList = getDates();
	  DateTime startDate = new DateTime("2011-04-30");
	  DateTime endDate = new DateTime("2011-05-21");
	  DateTime currentDay = dateList.get(0);
	  System.out.println(dateList.get(dateList.size()-1));
	  for(;dateCompare(currentDay,dateList.get(dateList.size()-1))<1;){
		  System.out.println(currentDay.getDayOfMonth());
		  currentDay = currentDay.plusDays(1);
	  }
  }
}
