package edu.cs424.p4.central.components;

import java.util.HashMap;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;
import processing.core.PApplet;


public  class Button extends Panel implements TouchEnabled
{	
	public boolean isPressed = false;
	private boolean isVisible = false;
	
	int roundCorners = 5;
	float alpha = 150;
	private String text;
	public int textSize = 8;
	EnumColor selectedTextColor = EnumColor.WHITE;

	public Button(float x0, float y0, float width, float height,
			float parentX0, float parentY0,String text,boolean isVisible) 
	{
		super(x0, y0, width, height, parentX0, parentY0);
		this.text = text;
		this.isVisible = isVisible;
	}
	
	public void setup()
	{
		
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {
		
		if(event == MouseMovements.MOUSEDOWN)
		{
			isPressed = !isPressed;
			
		}		
		return false;
	}
	

	
	@Override
	public void draw() 
	{
		if( isVisible)
		{			
			pushStyle();		
			if(isPressed)
			{
				fill(EnumColor.DARK_GRAY, alpha);
				stroke(EnumColor.BLACK);
				rect(0, 0, width, height, roundCorners, roundCorners, roundCorners, roundCorners);	
				fill(selectedTextColor);
			}
			else
			{
				fill(EnumColor.GRAY, alpha);
				stroke(EnumColor.BLACK);
				rect(0, 0, width, height, roundCorners, roundCorners, roundCorners, roundCorners);	
				fill(EnumColor.BLACK);
			}			
			textSize(textSize);
			textAlign(PApplet.CENTER, PApplet.CENTER);
			text(text, width/2f, height/2f);			
			
			popStyle();
		}		
	}

	
	@Override
	public boolean containsPoint(float x, float y) {
		if(isVisible)
			return super.containsPoint(x, y);
		else
			return false;
	}
	
	public void setVisibilty(boolean isVisible)
	{
		this.isVisible = isVisible;
		
	}
	
	public void setPressed(boolean isPressed)
	{
		this.isPressed = isPressed;
		
	}
	
	public void setSelectedTextColor(EnumColor selectedTextColor)
	{
		
			this.selectedTextColor = selectedTextColor;
		
	}
	
	public void setRoundedCorners(boolean isRoundedCorners)
	{		
		if(!isRoundedCorners)
		{
			this.roundCorners  = 0;
		}
		
	}
	
	
	public String getText()
	{
		return text;
	}
	
	// add css type settings
	

}
