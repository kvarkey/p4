package edu.cs424.p4.central.components;

import java.util.HashMap;

import processing.core.PConstants;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;


public class Keyboard extends Panel implements TouchEnabled {

	private HashMap<Character, Button> keyboardBUtton = new HashMap<Character, Button>();
	String searchQuery = "";

	public Keyboard(float x0, float y0, float width, float height, float parentX0, float parentY0) 
	{
		super(x0, y0, width, height, parentX0, parentY0);

	}

	String[] row1 = { "q", "w", "e", "r", "t", "y", "u", "i", "o", "p" };
	String[] row2 = { "a", "s", "d", "f", "g", "h", "j", "k", "l" };
	String[] row3 = { "z", "x", "c", "v", "b", "n", "m" };
	String[] row4 = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
	String[] row5 = {"(" ,")", " AND "," OR ","Apply"};

	Button[] buttons = new Button[43];

	public void setup() {
		int count = 0;

		for (String val : row1) {
			buttons[count] = new Button(27 + count * (20), 5+25, 15, 15, x0, y0,val,true);
			buttons[count].setRoundedCorners(false);
			count++;
		}

		int count1 = 0;
		for (String val : row2) {
			buttons[count] = new Button(37 + count1 * (20), 25+25, 15, 15, x0, y0,val,true);
			buttons[count].setRoundedCorners(false);
			count++;
			count1++;
		}

		count1 = 0;
		for (String val : row3) {
			buttons[count] = new Button(57 + count1 * (20), 45+25, 15, 15, x0, y0,val,true);
			buttons[count].setRoundedCorners(false);
			count++;
			count1++;
		}

		buttons[count] = new Button(27, 45+25, 25, 15, x0, y0,"_",true);
		buttons[count].setRoundedCorners(false);
		count++;
		buttons[count] = new Button(57 + count1 * (20), 45+25, 25, 15, x0, y0,"DEL",true);
		buttons[count].setRoundedCorners(false);

		count1 = 0;
		count++;
		for (String val : row4) {
			buttons[count] = new Button(27 + count1 * (20), 65+25, 15, 15, x0, y0,val,true);
			buttons[count].setRoundedCorners(false);
			count++;
			count1++;
		}

		count1 = 0;
		for (String val : row5) {
			buttons[count] = new Button(27 + count1 * (40), 85+25, 35, 15, x0, y0,val,true);
			buttons[count].setRoundedCorners(false);
			count++;
			count1++;
		}


	}

	@Override
	public void draw() {

		pushStyle();
		noStroke();
		background(EnumColor.BLACK);
		fill(EnumColor.GRAY_EVEN, 200);		
		rect(0, 0, width, 25);
		
		fill(EnumColor.WHITE);
		textAlign(PConstants.LEFT, PConstants.CENTER);
		text(searchQuery, 0,0,width, 25);

		for (Button but : buttons) {
			but.draw();

		}
		
		popStyle();
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event)
	{

		if( event == MouseMovements.MOUSEDOWN)
		{
			for (Button but : buttons)
			{
				if (but.containsPoint(x, y))
				{
					if(but.getText().equals("Apply"))
					{
						System.out.println("Keyboard.touch()");
					}
					if (but.getText().compareTo("_") == 0) 
					{
						searchQuery = searchQuery + " ";
					} 
					else if (but.getText().compareTo("DEL") == 0 && searchQuery.length() > 0) 
					{
						searchQuery = searchQuery.substring(0, searchQuery.length() - 1);
					} 
					else if (but.getText().compareTo("DEL") != 0) 
					{
						searchQuery = searchQuery + but.getText();
					}

				}
			}
		}

		return false;
	}

}
