package edu.cs424.p4.central.components;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import processing.core.PConstants;

import edu.cs424.p4.central.EnumColor;
import edu.cs424.p4.central.Panel;
import edu.cs424.p4.central.TouchEnabled;
import edu.cs424.p4.central.TouchListener.MouseMovements;

public class ListPanel extends Panel implements TouchEnabled
{
	int noOfRows,heightRow;
	int startIndex = 0;

	private ArrayList<String> rowData;
	private Set<String> selectedList = new HashSet<String>();

	List<Row> rowList;

	EnumColor oddColor,evenColor,selectedColor;
	Button topButton,bottomButton;
	boolean isMultiSelect;

	public ListPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0,int noOfRows,ArrayList<String> rowData,boolean isMultiSelect,EnumColor selectedColor) {

		super(x0, y0, width, height, parentX0, parentY0);

		this.noOfRows = noOfRows;
		this.rowData = rowData;
		this.isMultiSelect = isMultiSelect;
		rowList = new ArrayList<ListPanel.Row>();
		this.selectedColor = selectedColor;

	}

	@Override
	public boolean touch(float x, float y, MouseMovements event)
	{
		if(topButton.containsPoint(x, y))
			if(startIndex > 0 )
				startIndex--;

		if(bottomButton.containsPoint(x, y))
			if( startIndex < rowData.size() - noOfRows )
				startIndex++;

		for(Row row : rowList)
		{
			if(row.containsPoint(x, y))
			{
				if (selectedList.contains(row.getText()))
				{
					selectedList.remove(row.getText());
				}
				else
				{
					if(isMultiSelect)
					{
						selectedList.add(row.getText());
					}
					else
					{
						selectedList.clear();
						selectedList.add(row.getText());
					}
				}

				break;
			}
		}
		return false;
	}

	@Override
	public void setup()
	{
		topButton = new Button(0, 0, width, 15, x0, y0, "TOP", true);
		topButton.setup();
		topButton.setPressed(true);

		bottomButton = new Button(0, height-15, width, 15, x0, y0, "BOTTOM", true);
		bottomButton.setup();
		bottomButton.setPressed(true);

		heightRow = (int) ((height - 30)/noOfRows);
		oddColor = EnumColor.GRAY_ODD;
		evenColor = EnumColor.GRAY_EVEN;

	}

	@Override
	public void draw()
	{
		rowList.clear();
		background(EnumColor.BLACK);

		for(int i = startIndex,j = 0 ; i < rowData.size() && j < noOfRows ; i++,j++ )
		{
			EnumColor tempColor,tempText = EnumColor.BLACK;
			if(j % 2 == 0)
				tempColor = evenColor;
			else
				tempColor = oddColor;

			if(selectedList.contains( rowData.get(i)) )
			{
				tempColor = selectedColor;
				tempText = EnumColor.WHITE;
			}

			Row tempRow = new Row( 0, 15+(heightRow *j), width, heightRow, x0, y0, rowData.get(i), tempColor, tempText);
			tempRow.setup();
			tempRow.draw();
			rowList.add(tempRow);
		}

		topButton.draw();
		bottomButton.draw();
	}



	public Set<String> getSelectedList() {
		return selectedList;
	}

	public void setRowData(ArrayList<String> rowData) {
		this.rowData = rowData;
	}

	public void setDefaultSelected(int item)
	{
		if(rowData != null && !rowData.isEmpty())
			selectedList.add(rowData.get(item));
	}



	class Row extends Panel
	{

		String text;
		EnumColor bg,textColor;


		public Row(float x0, float y0, float width, float height,
				float parentX0, float parentY0,String text,EnumColor bgColor,EnumColor textColor) {
			super(x0, y0, width, height, parentX0, parentY0);
			this.text = text;
			this.bg = bgColor;
			this.textColor = textColor;
		}


		@Override
		public void setup() {
			// TODO Auto-generated method stub

		}

		@Override
		public void draw() 
		{
			pushStyle();
			background(bg);
			fill(textColor);
			textAlign(PConstants.CENTER, PConstants.CENTER);
			text(text, 0f, 0f, width, height);
			popStyle();
		}
		public String getText()
		{
			return text;
		}

	}



}
