package edu.cs424.p4.central;

public enum EnumColor 
{

	SOMERANDOM (133,133,1),
	DARK_GRAY (105,105,105),
	GRAY(190,190,190),
	GRAY_EVEN(135,135,135),
	GRAY_ODD(150,150,150),
	WHITE(255,255,255),
	BLACK(0,0,0),
	GOLD(255,215,0),
	RED(255,153,204),
	GRAPH1(32,129,93),
	GRAPH2(255, 80, 80);//GRAPH2(83,35,0);
	
	int color;
	private EnumColor(int r,int g , int b) 
	{
		color = Main.main.color(r, g, b);
	}
	
	public int getValue()
	{
		return color;
	}
}
