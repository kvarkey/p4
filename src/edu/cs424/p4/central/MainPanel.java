package edu.cs424.p4.central;

import edu.cs424.p4.central.TouchListener.MouseMovements;
import edu.cs424.p4.central.components.Button;
import edu.cs424.p4.central.components.ListPanel;
import edu.cs424.p4.db.DB;
import edu.cs424.p4.panels.EventPanel;
import edu.cs424.p4.panels.FIlterPanel;
import edu.cs424.p4.panels.FloatingKeyboard;
import edu.cs424.p4.panels.GraphPanel;
import edu.cs424.p4.panels.InfoPanel;
import edu.cs424.p4.panels.MapPanel;
import edu.cs424.p4.panels.MapPanel;
import edu.cs424.p4.panels.NewMapPanel;
import edu.cs424.p4.panels.SliderPanel;
import edu.cs424.p4.pubsub.PubSub.Event;
import edu.cs424.p4.pubsub.PubSub;
import edu.cs424.p4.pubsub.Subscribe;
import static edu.cs424.p4.central.AppConstants.*;



public class MainPanel extends Panel implements TouchEnabled,Subscribe
{	

	
	public static NewMapPanel mapPanel;
	//MapPanel mapPanel;

	public static GraphPanel graphPanel;
	FIlterPanel filterPanel;
	InfoPanel infoPanel;
	SliderPanel sliderPanel;
	FloatingKeyboard fk;
	EventPanel eventPanel;
	public static Button dayButton;
	boolean showFloatingWindow = false;



	public MainPanel(float x0, float y0, float width, float height,
			float parentX0, float parentY0) 
	{
		super(x0, y0, width, height, parentX0, parentY0);

		// TODO Auto-generated constructor stub
	}

	public boolean touch(int ID,float x, float y, MouseMovements event)
	{
		// if we are showing floating direct all the touch here
		if(showFloatingWindow)
		{
			fk.touch(x, y, event);
		}
		else
		{
			if(event == MouseMovements.MOUSEDOWN)
			{
				propagateTouch(x, y, event);			
			}		
			if( mapPanel.containsPoint(x, y) )
			{
				mapPanel.touch(ID,x, y, event);
			}

			if( sliderPanel.containsPoint(x, y))
			{
				sliderPanel.touch(ID,x, y, event);
			}
		}

		return false;
	}

	@Override
	public boolean touch(float x, float y, MouseMovements event) {

		return false;
	}

	public void setup() 
	{	

		// dont add map panel to addTouchSubscriber

		// we are going to handle map touch seperately
		DB db = new DB();
		db.checking();
		
		mapPanel = new NewMapPanel(mapX, mapY, mapWidth, mapHeight, x0, y0);
		//mapPanel = new MapPanel(mapX, mapY, mapWidth, mapHeight, x0, y0);

		mapPanel.setup();


		graphPanel = new GraphPanel(graphX, graphY, graphWidth, graphHeight, x0, y0);
		graphPanel.setup();		
		addTouchSubscriber(graphPanel);

		filterPanel = new FIlterPanel(filterPanelX, filterPanelY, filterPanelWidth, filterPanelHeight, x0, y0);
		filterPanel.setup();
		addTouchSubscriber(filterPanel);

		infoPanel = new InfoPanel(infoPanelX, infoPanelY, infoPanelWidth, infoPanelHeight, x0, y0);
		infoPanel.setup();
		addTouchSubscriber(infoPanel);

		// dont add slider panel to addTouchSubscriber
		// we are going to handle slider touch seperately
		sliderPanel = new SliderPanel(sliderX,sliderY,sliderWidth,sliderHeight,x0,y0);
		sliderPanel.setup();

		dayButton = new Button(zoomInButtonX-6,zoomInButtonY+65,sliderWidth/16-4,15,x0,y0,"Day",true);
		dayButton.setup();
		addTouchSubscriber(dayButton);

		fk = new FloatingKeyboard(0, 0, width, height, x0, y0);
		fk.setup();

		PubSub.subscribeEvent(Event.SAVE_QUERY, this);
		PubSub.subscribeEvent(Event.SAVE_USER, this);
		PubSub.subscribeEvent(Event.CLOSE_FLOATING_WINDOW, this);
		
		eventPanel = new EventPanel(EventPanelX, EventPanelY, EventPanelWidth, EventPanelHeight, x0, y0);
		eventPanel.setup();
	}

	@Override
	public void draw() 
	{	
		background(EnumColor.GRAY);	
		mapPanel.draw();		
		graphPanel.draw();
		filterPanel.draw();
		infoPanel.draw();
		sliderPanel.draw();
		dayButton.draw();
		//eventPanel.draw();

		if(showFloatingWindow)
			fk.draw();

	}

	@Override
	public void receiveNotification(Event eventName, Object... object)
	{
		if( eventName == Event.CLOSE_FLOATING_WINDOW)
		{
			showFloatingWindow = false;
		}
		else if ( eventName == Event.SAVE_USER)
		{
			showFloatingWindow = true;
		}

	}

}
