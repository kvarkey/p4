package edu.cs424.p4.central;



public class AppConstants {
    
	public static int fullScreenWidth = 1360;
	public static int fullScreenHeight = 384;
	
	public static int mapX = 0;
	public static int mapY = 0;
	public static int mapWidth = 580;
	public static int mapHeight = 334;
	
	public static int sliderX = 10;
	public static int sliderY = 335;
	public static int sliderWidth = 580;
	public static int sliderHeight = 50;
	
	public static int graphX = 620;
	public static int graphY = 0;
	public static int graphWidth = 340;
	public static int graphHeight = 187; 
	//public static int graphWidth = 500;
	//public static int graphHeight = 200;
	public static int graphXBuffer = 20;
	public static int graphYBuffer = 20;
	
	public static int infoPanelX = 620;
	public static int infoPanelY = 197;
	public static int infoPanelWidth = 340;
	public static int infoPanelHeight = 192;
	
	public static int filterKeywordListX  = 30;
	public static int filterKeywordListY  = 20;
	public static int filterKeywordWidth  = 230;
	public static int filterKeywordHeight = 140;
	
	
	public static int andButtonX = 271;
	public static int andButtonY =20;
	public static int andButtonWidth = 40;
	public static int andButtonHeight = 20;
	
	public static int orButtonX = 271;
	public static int orButtonY = 42;
	
	public static int applyButtonX =  271;
	public static int applyButtonY = 140;

	
	public static int keywordButtonX =  40;
	public static int keywordButtonY = 163;
	public static int keywordButtonWidth = 85;
	public static int keywordButtonHeight = 20;
	
	
	//spacing 3
	
	public static int filterPanelX = 1000;
	public static int filterPanelY = 197;
	public static int filterPanelWidth = 340;
	public static int filterPanelHeight = 192;
	
	
	/*
	 * Info panel inner measurements
	 * */
	
	
	public static int infoInnerPanelX = 8;
	public static int infoInnerPanelY = 8;
	public static int infoInnerPanelWidth = 324;
	public static int infoInnerPanelHeight = 152;
	
	//user list
	public static int uniqueUsersX = 5;
	public static int uniqueUsersY = 24;
	public static int uniqueUsersWidth = 115;
	public static int uniqueUsersHeight = 106;
	
	// tweet list
	public static int userTweetsX = 180;
	public static int userTweetsY = 24;
	public static int userTweetsWidth = 132;
	public static int userTweetsHeight = 106;
	
	public static int saveUserX = 95;
	public static int saveUserY = 135;
	public static int saveUserWidth = 80;
	public static int saveUserHeight = 15;
	
	public static int goBackGraphInfoX = 5;
	public static int goBackGraphInfoY = 135;
	public static int goBackGraphInfoWidth = 80;
	public static int goBackGraphInfoHeight = 15;
	
	
	/**
	 *components inside infoInnerPanel 
	 */
	
	public static int graphInfoLabelX = 6;
	public static int graphInfoLabelY = 7;
	public static int graphInfoLabelWidth = 67;
	public static int graphInfoLabelHeight = 15;
	
	
	public static int graphAttributeX = 6;
	public static int graphAttributeY = 32;
	public static int graphAttributeWidht = 79;
	public static int graphAttributeHeight = 15;
	
	//spacing 5;
	
	public static int weatherX = 216;
	public static int weatherY = 32;
	public static int weatherWidth = 97;
	public static int weatherHeight = 30;
	
	public static int showUsersX = 216;
	public static int showUsersY = 132;
	public static int showUsersWidth = 80;
	public static int showUsersHeight = 15;
	
			
	
	
	//Given Lat-Long of map image
	public static float topLeftLat = 42.3017f;
	public static float topLeftLon = 93.5673f;
	public static float bottomRightLat = 42.1609f;
	public static float bottomRightLon = 93.1923f;
	
	//Map buttons
	public static int zoomInButtonX = 534;
	public static int zoomInButtonY = 268;
	public static int panLeftButtonX = 511;
	public static int panLeftButtonY = 278;
	public static int panRightButtonX = 557;
	public static int panRightButtonY = 278;

	public static int zoomButtonWidth = 20;
	public static int zoomButtonHeight = 20;
	
	
	//Marker stuff
	public static final int DEFAULT_FILL_COLOR = -10263709;
	public static final int DEFAULT_STROKE_COLOR = -11382190;
	public static final int HIGHLIGHTED_FILL_COLOR = -4640977;
	public static final int HIGHLIGHTED_STROKE_COLOR = -7068379;

	public static final int DEFAULT_STROKE_WEIGHT = 1;
	
	
	//timeOfDay
	public static final String TIMEOFDAY_MORNING = "00:00,09:00";
	public static final String TIMEOFDAY_DAY = "09:00,18:00";
	public static final String TIMEOFDAY_NIGHT = "18:00,23:59";
	public static final String TIMEOFDAY_COLUMN = "hour";
	
	
	//Color for graph1
	public static final int GRAPH1_FILL_COLOR = Main.main.color(55, 208, 134);//Integer.parseInt("#37d086".replaceFirst("#", ""), 16);
	public static final int GRAPH1_HIGHLIGHTED_FILL_COLOR = Main.main.color(35, 150, 94);//Integer.parseInt("#23965E".replaceFirst("#", ""), 16);
	//Color for graph2
	public static final int GRAPH2_FILL_COLOR = Main.main.color(255, 80, 80);//Integer.parseInt("#ff5050".replaceFirst("#", ""), 16);
	public static final int GRAPH2_HIGHLIGHTED_FILL_COLOR = Main.main.color(217, 0, 0);//Integer.parseInt("#D90000".replaceFirst("#", ""), 16);
		
	public static final int GRAPHPLOT_MORNING = 0;
	public static final int GRAPHPLOT_DAY = 1;
	public static final int GRAPHPLOT_NIGHT = 2;
	
	// event panel
	
	public static int EventPanelX = 1000;
	public static int EventPanelY = 0;
	public static int EventPanelWidth = 340;
	public static int EventPanelHeight = 192;
	
	
}
