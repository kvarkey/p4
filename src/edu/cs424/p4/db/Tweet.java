package edu.cs424.p4.db;

import java.util.Date;

public class Tweet implements Comparable<Tweet>
{
	public int userid;
	public  Date datetime;
	public  float lat;
	public  float lon;
	public String tweet;
	
	Tweet(int id, Date dt, float lat, float lon, String tweet)
	{
		userid = id;
		datetime = dt;
		this.lat = lat;
		this.lon = lon;
		this.tweet = tweet;
	}

	@Override
	public int compareTo(Tweet o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}
}
