package edu.cs424.p4.db;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;

import edu.cs424.p4.central.AppConstants;
import edu.cs424.p4.central.components.Slider;
import edu.cs424.p4.pubsub.PubSub.Event;

public class DBAider
{
	public static HashMap<Integer,ArrayList<Tweet>> userMap;//for graph 1
	public static ArrayList<ArrayList<Integer>> countArray;
	public static ArrayList<ArrayList<Integer>> dateArray;
	public static HashMap<Integer,ArrayList<Tweet>> userMap2;//for graph 2
	public static ArrayList<Integer> graph1CountArray;
	public static ArrayList<Integer> graph1DateArray;
	public static ArrayList<Integer> graph2CountArray;
	public static ArrayList<Integer> graph2DateArray;
	static String tod=null;
	static String eventName="UPDATE_GRAPH1";
	
	public static GraphPlotData getUniqueUser(String keywords,String people,String joinOperator,String timeOfDay,Event graphType)
	{
		tod = timeOfDay;
		eventName = graphType.name();
		return getUniqueUser(keywords,people,joinOperator);
	}
	
	public static GraphPlotData getUniqueUser(String keywords,String people,String joinOperator)
	{
		ArrayList<String> uniqueUsersList = new ArrayList<String>();
		System.out.println("keywords:"+keywords);
		System.out.println("people:"+people);
		System.out.println("startd:"+Slider.currentMinDate.toDate());
		System.out.println("startd:"+Slider.currentMaxDate.toDate());
		System.out.println("join:"+joinOperator);
		String qAND = " AND ";
		String query = "SELECT * FROM temp WHERE "+getDates(Slider.currentMinDate,Slider.currentMaxDate,tod)+qAND+ getKeyWords(keywords)+getPeople(people);
		System.out.println(query);
		
		int uniqueUsersCount=0;
		int totalTweets=0;
		System.out.println("eventName:"+eventName);
		if(eventName.equalsIgnoreCase("UPDATE_GRAPH1"))
		{
			userMap = new HashMap<Integer, ArrayList<Tweet>>();
			DB.db.query(query);
			processResults(userMap);
			uniqueUsersList = new ArrayList<String>();
			for(Integer id : userMap.keySet())
			{
				String s = ""+id+" : "+userMap.get(id).size();
				uniqueUsersList.add(s);
				totalTweets+=userMap.get(id).size();
			}
			uniqueUsersCount = userMap.size();
		}
		else
		{
			userMap2 = new HashMap<Integer, ArrayList<Tweet>>();
			DB.db.query(query);
			processResults(userMap2);
			for(Integer id : userMap2.keySet())
			{
				String s = ""+id+" : "+userMap2.get(id).size();
				uniqueUsersList.add(s);
				totalTweets+=userMap2.get(id).size();
			}
			uniqueUsersCount = userMap2.size();
		}
				
		
		//String query1 = "SELECT * FROM temp WHERE "+getDates(Slider.currentMinDate,Slider.currentMaxDate,null)+qAND+ getKeyWords(keywords);
		System.out.println("Size : "+userMap.size());
		
		/*uniqueUsersList.add("Kevin : 1");
		uniqueUsersList.add("someone : 1001");
		uniqueUsersList.add("someone : 1002");
		uniqueUsersList.add("someone : 1003");
		uniqueUsersList.add("someone : 1004");
		uniqueUsersList.add("someone : 1005");
		uniqueUsersList.add("someone : 1006");
		uniqueUsersList.add("someone : 1007");*/
		System.out.println("uniqueUsersList:"+uniqueUsersList.size());
		System.out.println("totalTweets with keywords:"+totalTweets);
		System.out.println("uniqueUsersCount:"+uniqueUsersCount);
		getDataForGraph(keywords);

		GraphPlotData plotData = new GraphPlotData(uniqueUsersList, totalTweets, 12, uniqueUsersCount, null);
		return plotData;
	}
	
	public static void processResults(HashMap<Integer,ArrayList<Tweet>> map)
	{
		while(DB.db.next())
		{
			int id = DB.db.getInt(2);
			Date dt = DB.db.getDate(3);
			float lat = DB.db.getFloat(4);
			float lon = DB.db.getFloat(5);
			String twt = DB.db.getString(6);
			if(map.containsKey(id))
			{
				ArrayList<Tweet> tweetlist = map.get(id);
				Tweet t = new Tweet(id,dt,lat,lon,twt);
				tweetlist.add(t);
			}
			else
			{
				ArrayList<Tweet> tweetlist = new ArrayList<Tweet>();
				Tweet t = new Tweet(id,dt,lat,lon,twt);
				tweetlist.add(t);
				map.put(id, tweetlist);
			}
		}
	}
	
	/**
	 * To init data on the map
	 */
	public static void initData()
	{
		String q = "SELECT * FROM temp WHERE datetime BETWEEN '2011-05-16 00:00 ' AND '2011-05-16 23:59' AND  keywords MATCH ' cold'";
		DB.db.query(q);
		userMap = new HashMap<Integer, ArrayList<Tweet>>();
		processResults(userMap);
		String q2 = "SELECT day,count(*) FROM temp WHERE datetime BETWEEN '2011-05-14 00:00 ' AND '2011-05-16 23:59' AND  keywords MATCH ' cold' GROUP BY day";
		//Slider.currentMaxDate = new DateTime("2011-05-16");
		//Slider.currentMinDate = Slider.currentMaxDate;
		//getDataForGraph("cold");
		processDataForGraph(q2, null);
		eventName="UPDATE_GRAPH2";
		processDataForGraph(q2, null);
	}

	public static ArrayList<String> getUserTweets(String userID,Event graphID)
	{
		System.out.println("DBAider.getUserTweets()" + "getting the data for " + userID);
		
		ArrayList<String> userTweets = new ArrayList<String>();
		ArrayList<Tweet> tw;
		
		if( graphID == Event.UPDATE_GRAPH1)
			tw = userMap.get(new Integer( userID.split(" : ")[0]));
		else
			tw = userMap2.get(new Integer( userID.split(" : ")[0]));
		
		
		for (Tweet t : tw)
		{
			userTweets.add(t.tweet);
		}
			
		
		return userTweets;
	}
	
	public static String getKeyWords(String k)
	{
		StringBuffer s = new StringBuffer();
		s.append(" keywords MATCH '"+k+"'");
		return s.toString();
	}
	
	public static String parseKeywords(String k, String j)
	{
		StringBuffer s = new StringBuffer();
		s.append(" keywords LIKE ");
		String sp[] = k.trim().split(j);
		System.out.println(sp.length);
		for(int i=0;i<sp.length;i++)
		{
			if(i==0)
				s.append(" '%"+sp[i].trim()+"%' ");
			else
				s.append(" union SELECT * FROM tweet WHERE keywords MATCH '%"+sp[i].trim()+"%'");
		}
		//System.out.println("qpart:"+s.toString());
		return s.toString();
	}
	
	//Methods to handle date and time.
	public static String getDates(DateTime c1, DateTime c2, String timeOfDay)
	{
		StringBuffer s = new StringBuffer();
		if(c1.compareTo(c2)==0)
		{
			int cyear = c1.getYear();
			int cmonth = c1.getMonthOfYear();
			int cday = c1.getDayOfMonth();
			String cm,cd;
			if(cmonth<10)
				cm ="0"+cmonth;
			else
				cm = ""+cmonth;
			if(cday<10)
				cd ="0"+cday;
			else
				cd =""+cday;
			String cd1 = cyear+"-"+cm+"-"+cd;
			String[] time = getTimeOfDay(timeOfDay);
			s.append("datetime BETWEEN '"+cd1+" "+time[0]+"' AND ");
			s.append("'"+cd1+" "+time[1]+"'");
			return s.toString();
		}
		else
		{
			int cyear1 = c1.getYear();
			int cmonth1 = c1.getMonthOfYear();
			int cday1 = c1.getDayOfMonth();
			String cm1;
			String cd1;
			if(cmonth1<10)
				cm1 ="0"+cmonth1;
			else
				cm1 = ""+cmonth1;
			if(cday1<10)
				cd1="0"+cday1;
			else
				cd1=""+cday1;
			
			int cyear2 = c2.getYear();
			int cmonth2 = c2.getMonthOfYear();
			int cday2 = c2.getDayOfMonth();
			String cm2;
			String cd2;
			if(cmonth2<10)
				cm2 ="0"+cmonth2;
			else
				cm2 = ""+cmonth2;
			if(cday2<10)
				cd2 ="0"+cday2;
			else
				cd2 =""+cday2;
			String[] time = getTimeOfDay(timeOfDay);
			/*String cd1 = "'"+cyear1+"-"+cmonth1+"-"+cday1+" "+time[0]+"'";
			cd1 += " AND '"+cyear1+"-"+cmonth1+"-"+cday1+" "+time[1]+"'";
			String cd2 = " datetime between '";
			cd2 += c2.getYear()+"-"+c2.getMonthOfYear()+"-"+c2.getDayOfMonth()+" "+time[0];
			cd2 += "' AND '"+
			s.append(" datetime between "+cd1+" AND "+cd2);*/
			s.append(" datetime between ");
			s.append("'"+cyear1+"-"+cm1+"-"+cd1+" "+time[0]+"'");
			s.append(" AND ");
			s.append("'"+cyear2+"-"+cm2+"-"+cd2+" "+time[1]+"'");
			return s.toString();
		}
	}
	

	public static String[] getTimeOfDay(Integer timeOfDay)
	{
		String[] a = {" 00:00 ","23:59"};
		if(timeOfDay == null)
		{
			return a;
		}
		else
		{
			if(timeOfDay == 0)//morning
			{
				a = AppConstants.TIMEOFDAY_MORNING.split(",");
			}
			else if(timeOfDay == 1)//day
			{
				a = AppConstants.TIMEOFDAY_DAY.split(",");
			}
			else//night
			{
				a = AppConstants.TIMEOFDAY_NIGHT.split(",");
			}
		}
		return a;
	}
	
	public static String[] getTimeOfDay(String timeOfDay)
	{
		String[] a = {" 00:00 ","23:59"};
		if(timeOfDay == null)
		{
			return a;
		}
		else
		{
			if(timeOfDay.equalsIgnoreCase("morning"))
			{
				a = AppConstants.TIMEOFDAY_MORNING.split(",");
			}
			else if(timeOfDay.equalsIgnoreCase("day"))
			{
				a = AppConstants.TIMEOFDAY_DAY.split(",");
			}
			else if(timeOfDay.equalsIgnoreCase("night"))
			{
				a = AppConstants.TIMEOFDAY_NIGHT.split(",");
			}
		}
		return a;
	}
	
	public static String getPeople(String people)
	{
		if(people==null || people.equalsIgnoreCase(""))
			return "";
		String[] splitNames = people.split(",");
		return " AND userid IN ("+ getPeopleID(splitNames)+")";
	}
	
	public static String getPeopleID(String[] inputNames)
	{
		String outputIDs="";
		String _sql = "SELECT * FROM user_info WHERE user_name IN (";
		for(int i=0;i<inputNames.length;i++)
		{
			if(i!=inputNames.length-1)
			{
				_sql += "'"+inputNames[i]+"',";
			}
			else
				_sql += "'"+inputNames[i]+"')";
		}
		System.out.println("Fetching people names:");
		System.out.println(_sql);
		
		DB.db.query(_sql);
		ArrayList<String> listIDs = new ArrayList<String>();
		while(DB.db.next())
		{
			String id = new Integer(DB.db.getInt(1)).toString();
			listIDs.add(id);
		}
		for(int i=0;i<listIDs.size();i++)
		{
			if(i!=listIDs.size()-1)
				outputIDs+= "'" + listIDs.get(i).toString()+ "'" + ",";
			else
				outputIDs+="'" + listIDs.get(i).toString()+ "'";
		}
		return outputIDs;
	}
	
	
	public static void saveNewUser(String oldName,String newName)
	{
		System.out.println("DBAider.saveNewUser()" + oldName + " " + newName);

		if( isNumber(oldName))
		{
			String _sql = "INSERT INTO user_info VALUES (" +oldName + ",'" + newName + "')";
			System.out.println("DBAider.saveNewUser()" + _sql);
			if(DB.db.executeUpdate(_sql) > 0)
			{

			}
		}		
	}

	public static boolean isNumber(String input) {
		try {
			Integer.parseInt( input );
			return true;
		}
		catch( Exception e ) {
			return false;
		}
	}
	
	
	public static ArrayList<String> getSavedUserNames()
	{
		ArrayList<String> userList = new ArrayList<String>();
		String query = "select * from user_info";
		DB.db.query(query);
		
		while(DB.db.next())
		{
			userList.add(DB.db.getString("user_name"));
		}
		
		return userList;
	}
	
	//Need to write a method to convert graphfilters to a format that can be stored.
	public static boolean saveQuery(String key, GraphFilterParameters val) 
	{
		String _sql = "INSERT INTO query VALUES("+key+",";
		if(DB.db.executeUpdate(_sql) > 0)
		{
			System.out.println("Insert successful");
			return true;
		}
		else
			return false;
	}
	
	
	// key is the name of the query 
	public static  HashMap<String, GraphFilterParameters> getSavedQueryDetails()
	{
		return null;

	}
	
	public static void getDataForGraph(String keywords)
	{
		String t=null,q;
		String _sql_keywords="";
		System.out.println("Slider.currentMaxDate::"+Slider.currentMaxDate);
		System.out.println("Slider.currentMinDate::"+Slider.currentMinDate);
		if(Slider.currentMaxDate.compareTo(Slider.currentMinDate)==0)//If a single day is selected, query by timeofday
		{
			t = " AND date="+Slider.currentMinDate.getYear()+"-"+Slider.currentMinDate.getMonthOfYear()+"-"+Slider.currentMinDate.getDayOfMonth();
		}
		if(keywords!=null)
			_sql_keywords = " WHERE "+getKeyWords(keywords);
		if(t==null)//If single day is not selected, query all days.
		{
			String d1 = getSQLFormatDate(Slider.currentMinDate);
			String d2 = getSQLFormatDate(Slider.currentMaxDate.plusDays(1));
			String qDate="";
			if(keywords==null)
				qDate=" WHERE ";
			else
				qDate=" AND ";
			qDate += " datetime BETWEEN "+d1+" AND "+d2;
			q = "SELECT day,count(*) FROM temp "+_sql_keywords+qDate+" GROUP BY day ORDER BY day";
			
		}
		else
		{
			String temp="";
			if(keywords!=null)
				temp=" AND ";
			else
				temp=" WHERE ";
			q = "SELECT day, count(*) FROM temp "+_sql_keywords+temp+" day="+getSQLFormatDate(Slider.currentMinDate)+ " GROUP BY "+AppConstants.TIMEOFDAY_COLUMN;
		}
		System.out.println(q);
		processDataForGraph(q,t);
	}
	
	public static void processDataForGraph(String _sql,String t)
	{
		ArrayList<Integer> cntArray = new ArrayList<Integer>();
		ArrayList<Integer> dtArray = new ArrayList<Integer>();
		if(Slider.currentMinDate==null && Slider.currentMaxDate==null)
		{
			Slider.currentMinDate = new DateTime("2011-05-16");
			Slider.currentMaxDate = new DateTime("2011-05-20");
		}
		ArrayList<Integer> actualDateArray = getRangeOfDates(Slider.currentMinDate,Slider.currentMaxDate);
		System.out.println(_sql);
		DB.db.query(_sql);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
		int day=0;
		while(DB.db.next())
		{
			/*if(t==null){
			Date d = df.parse(DB.db.getString("day"));
			DateTime dt = new DateTime(d);
			dtArray.add(dt.getDayOfMonth());
			}
			else
			{
				String cnt = DB.db.getString(1); 
				dtArray.add(getHourType(cnt));
			}*/
			Date d = df.parse(DB.db.getString("day"));
			DateTime dt = new DateTime(d);
			dtArray.add(dt.getDayOfMonth());
			cntArray.add(DB.db.getInt(2));
		}
		if(Slider.currentMinDate.compareTo(Slider.currentMaxDate)!=0 && !cntArray.isEmpty() && dtArray.size()!=actualDateArray.size()){
			ArrayList<ArrayList<Integer>> k;
			k = resolveDateConflicts(dtArray,actualDateArray,cntArray);
			cntArray = k.get(0);
			dtArray = k.get(1);
		}
		else if(Slider.currentMinDate.compareTo(Slider.currentMaxDate)==0)
		{
			if(cntArray.size()==1)
			{				
					cntArray.add(0);
					cntArray.add(0);				
			}
			else if(cntArray.size()==2)
			{
				cntArray.add(0);
			}
		}
		System.out.println("DateArraySize:"+dtArray.size());
		System.out.println("Count array size:"+cntArray.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		if(eventName.equalsIgnoreCase("UPDATE_GRAPH1"))
		{
			graph1CountArray = cntArray;
			graph1DateArray = dtArray;
			if(countArray==null )
			{
				countArray = new ArrayList<ArrayList<Integer>>();
				dateArray = new ArrayList<ArrayList<Integer>>();
			}
			if(countArray.size()>1)
			{
				graph2CountArray = countArray.get(1);
				graph2DateArray = dateArray.get(1);
			}
			assignArrayLists();
			
		}
		else
		{
			graph2CountArray = cntArray;
			graph2DateArray = dtArray;
			
			if(countArray==null )
			{
				countArray = new ArrayList<ArrayList<Integer>>();
				dateArray = new ArrayList<ArrayList<Integer>>();
			}
			if(countArray.size()>1)
			{
				graph1CountArray = countArray.get(0);
				graph1DateArray = dateArray.get(0);
			}
			
			assignArrayLists();
		}

		System.out.println("countArray::"+countArray);
		System.out.println("dateArray::"+dateArray);
		
	}
	
	public static int getHourType(String h)
	{
		if(h.equalsIgnoreCase("morning"))
			return AppConstants.GRAPHPLOT_MORNING;
		else if(h.equalsIgnoreCase("day"))
			return AppConstants.GRAPHPLOT_DAY;
		else if(h.equalsIgnoreCase("night"))
			return AppConstants.GRAPHPLOT_NIGHT;
		else
			return 0;
	}
	
	public static void assignArrayLists()
	{
		countArray = new ArrayList<ArrayList<Integer>>();
		dateArray = new ArrayList<ArrayList<Integer>>();
		if(graph1CountArray!=null && graph1DateArray!=null)
		{
			countArray.add(graph1CountArray);
			dateArray.add(graph1DateArray);
		}
		if(graph2CountArray!=null && graph2DateArray!=null)
		{
			countArray.add(graph2CountArray);
			dateArray.add(graph2DateArray);
		}
	}
	
	public static String getSQLFormatDate(DateTime c)
	{
		String d = "";
		int y = c.getYear();
		int m = c.getMonthOfYear();
		int day = c.getDayOfMonth();
		String addzero="";
		String addzero2="";
		if(m<10)
			addzero="0";
		if(day<10)
			addzero2="0";
		d = "'"+y+"-"+addzero+m+"-"+addzero2+day+"'";
		return d;
		
	}
	
	public static ArrayList<Integer> getRangeOfDates(DateTime startDate, DateTime endDate)
	{
		ArrayList<Integer> dates = new ArrayList<Integer>();
		  int days = Days.daysBetween(startDate, endDate.plusDays(1)).getDays();
		  for (int i=0; i < days; i++) {
		      DateTime d = startDate.withFieldAdded(DurationFieldType.days(), i);
		      dates.add(d.getDayOfMonth());
		  }
		  return dates;
	}
	
	public static ArrayList<ArrayList<Integer>> resolveDateConflicts(ArrayList<Integer> dtArray, ArrayList<Integer> actualDateArray, ArrayList<Integer> cntArray)
	{
		ArrayList<ArrayList<Integer>> retlist = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> resolvedList = new ArrayList<Integer>();
		ArrayList<Integer> newDateArray = new ArrayList<Integer>();
		int cnt=0;
		int valcount;
		System.out.println("date array :"+dtArray);
		System.out.println("actualDateArray  :"+actualDateArray);
		for(Integer a : actualDateArray )
		{
			if(cnt==dtArray.size()-1)
				break;
			if(a==dtArray.get(cnt))
			{
				resolvedList.add(cntArray.get(cnt));
				newDateArray.add(dtArray.get(cnt));
				cnt++;
			}
			else
			{
				resolvedList.add(0);
				newDateArray.add(a);
			}
		}
		System.out.println("Resolved list:"+resolvedList+":"+resolvedList.size());
		System.out.println("new date list:"+newDateArray+":"+newDateArray.size());
		retlist.add(resolvedList);
		retlist.add(newDateArray);
		return retlist;
	}
}
