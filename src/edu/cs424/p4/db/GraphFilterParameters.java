package edu.cs424.p4.db;

import java.util.List;
import java.util.Set;

public class GraphFilterParameters
{

	Set<String> keywords,people	;
	String timeOfDay;
	
	String joinOperator;
	
	public GraphFilterParameters( Set<String> keywords , Set<String> people , String joinOperator,String timeOfDay)
	{
		this.keywords = keywords;
		this.people = people;
		this.joinOperator = joinOperator;
		this.timeOfDay = timeOfDay;		
	}

	public Set<String> getKeywords() {
		return keywords;
	}

	public Set<String> getPeople() {
		return people;
	}

	public String getJoinOperator() {
		return joinOperator;
	}
	
	public String getTimeOfDay()
	{
		return timeOfDay;
	}
	
	


	

	
}
