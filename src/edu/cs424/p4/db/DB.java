package edu.cs424.p4.db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import edu.cs424.p4.central.Main;
import edu.cs424.p4.central.SettingsLoader;


public class DB 
{
	static SQLite db;
	
	  public DB()
	  {
	      db = new SQLite(Main.main, SettingsLoader.dir + File.separator + "example.db");
	  }

	static Connection conn;
	
	/*static {
		String sqliteDB = "jdbc:sqlite:" + SettingsLoader.dir + File.separator + "example.db";
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection( sqliteDB );
		}
		catch ( ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void someQuery(int uid) {
		String query = "";
		
		Statement stat;
		ResultSet res;
		
		
		try {
			stat = conn.createStatement();
			res = stat.executeQuery(query);
		}
		
		catch (Exception e) {
			// TODO: handle exception
		}
	}*/
	

	public void checking() // checks for DB Connection
	{
	    if(db.connect())
	    {
	      
	      db.query( "SELECT name as \"Name\" FROM SQLITE_MASTER where type=\"table\"" );
	      while (db.next())
	      {
	          //println( db.getString("Name") );
	      }
	    }
	  }

}
