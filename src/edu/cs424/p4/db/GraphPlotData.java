package edu.cs424.p4.db;

import java.util.ArrayList;
import java.util.List;

public class GraphPlotData 
{

	ArrayList<String>  uniqueUsersList;
	int tweetswithKeywords, totalTweets, uniqueUsers;
	float[][] latlong;
	
	public GraphPlotData(ArrayList<String>  uniqueUsersList,	int tweetswithKeywords,int totalTweets,
			int uniqueUsers, float[][] latlong)
	{		
		this.uniqueUsersList = uniqueUsersList;
		this.tweetswithKeywords = tweetswithKeywords;
		this.totalTweets = totalTweets;
		this.uniqueUsers = uniqueUsers;		
		this.latlong = latlong;		
	}

	public ArrayList<String> getUniqueUsersList() {
		return uniqueUsersList;
	}

	public int getTweetswithKeywords() {
		return tweetswithKeywords;
	}

	public int getTotalTweets() {
		return totalTweets;
	}

	public int getUniqueUsers() {
		return uniqueUsers;
	}

	public float[][] getLatlong() {
		return latlong;
	}
	
	
	
}
