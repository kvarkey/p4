package edu.cs424.p4.pubsub;

import edu.cs424.p4.pubsub.PubSub.Event;


public interface Subscribe {

	public void receiveNotification(Event eventName , Object... object);
}
