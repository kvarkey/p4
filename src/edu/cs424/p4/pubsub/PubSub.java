package edu.cs424.p4.pubsub;

import java.util.ArrayList;
import java.util.HashMap;

public class PubSub 
{
	public enum Event{
		FOCUS_GRAPH1,
		FOCUS_GRAPH2,
		UPDATE_GRAPH1,
		UPDATE_GRAPH2,
		UPDATE_SLIDER,
		SAVE_USER,
		SAVE_QUERY,
		CLOSE_FLOATING_WINDOW,
		NEW_USER_ADDED,
		NEW_QUERY_ADDED
	}
	
	private static HashMap<Event , ArrayList<Subscribe> > listners = new HashMap<Event, ArrayList<Subscribe>>();

	public static void subscribeEvent(Event eventName , Subscribe suscriber)
	{
		if(listners.containsKey(eventName))
		{
			listners.get(eventName).add(suscriber);
		}
		else
		{
			ArrayList<Subscribe> toStore = new ArrayList<Subscribe>();
			toStore.add(suscriber);

			listners.put(eventName, toStore);
		}
	}

	public static void publishEvent(Event eventName,Object... object)
	{
		if(listners.containsKey(eventName))
		{
			ArrayList<Subscribe> list = listners.get(eventName);
			for(Subscribe toPublish : list)
			{
				toPublish.receiveNotification(eventName,object);
			}

		}
		else
		{
			System.out.println("event not found");
		}
	}

}
